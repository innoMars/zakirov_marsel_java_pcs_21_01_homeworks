package ru.pcs.ates.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseDto {

    private Boolean success;

    private Object data;

    private List<String> error;
}
