package ru.pcs.ates.dto;

import lombok.*;
import ru.pcs.ates.models.Account;

import javax.persistence.Column;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountDto {

    private String firstName;

    private String lastName;

    private String email;

    private Long id;

    public AccountDto from(Account account) {
        return AccountDto.builder()
                .email(account.getEmail())
                .firstName(account.getFirstName())
                .lastName(account.getLastName())
                .id(account.getId())
                .build();
    }
}
