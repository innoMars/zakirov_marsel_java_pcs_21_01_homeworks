package ru.pcs.ates.controllers;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.pcs.ates.dto.ResponseDto;
import ru.pcs.ates.dto.SignUpForm;
import ru.pcs.ates.services.SignUpService;

@RequiredArgsConstructor
@Controller
@RequestMapping("/signUp")
public class SignUpController {

    private final SignUpService signUpService;

    @RequestMapping()
    public String getSignUpPage(Model model) {
        model.addAttribute("signUpForm", new SignUpForm());
        return "signUp";
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<ResponseDto> signUp(@RequestBody SignUpForm form) {
        if (form == null) {
            return ResponseEntity.badRequest().body(ResponseDto.builder()
                    .data(form)
                    .success(false)
                    .build());
        }
        signUpService.signUp(form);
        return ResponseEntity.ok(ResponseDto.builder()
                .data(form)
                .success(true)
                .build());
    }
}
