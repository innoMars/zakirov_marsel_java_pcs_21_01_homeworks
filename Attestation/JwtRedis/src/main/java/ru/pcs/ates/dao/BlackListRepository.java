package ru.pcs.ates.dao;

public interface BlackListRepository {

    void save(String token);

    boolean exists(String token);
}
