package ru.pcs.ates.services;

import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.pcs.ates.dao.AccountsRepository;
import ru.pcs.ates.dto.SignUpForm;
import ru.pcs.ates.models.Account;

@Service
@AllArgsConstructor
public class SignUpServiceImpl implements SignUpService {

    private final AccountsRepository accountsRepository;
    private final PasswordEncoder passwordEncoder;


    @Override
    public void signUp(SignUpForm form) {
        Account account = Account.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .email(form.getEmail())
                .password(passwordEncoder.encode(form.getPassword()))
                .state(Account.State.CONFIRMED)
                .role(Account.Role.USER)
                .build();

        accountsRepository.save(account);
    }
}
