package ru.pcs.ates.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.pcs.ates.models.Account;

import java.util.Optional;

public interface AccountsRepository extends JpaRepository<Account,Long> {
    Optional<Account> findByEmail(String email);
    Optional<Account> findByAccessToken(String token);
}
