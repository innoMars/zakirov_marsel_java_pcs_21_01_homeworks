package ru.pcs.ates.services;

import ru.pcs.ates.dto.AccountDto;
import ru.pcs.ates.dto.SignUpForm;
import ru.pcs.ates.models.Account;

public interface SignUpService {

    void signUp(SignUpForm form);
}
