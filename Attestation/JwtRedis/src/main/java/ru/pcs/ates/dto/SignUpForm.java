package ru.pcs.ates.dto;


import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
public class SignUpForm {

    private String firstName;

    private String lastName;

    private String password;

    private String email;
}
