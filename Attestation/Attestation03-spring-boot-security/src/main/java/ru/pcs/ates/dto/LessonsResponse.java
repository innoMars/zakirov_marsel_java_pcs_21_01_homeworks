package ru.pcs.ates.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LessonsResponse {

    private List<LessonDto> data;
}
