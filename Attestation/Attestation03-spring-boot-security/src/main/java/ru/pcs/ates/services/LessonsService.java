package ru.pcs.ates.services;




import ru.pcs.ates.dto.LessonDto;

import java.util.List;

public interface LessonsService {
    List<LessonDto> getLessons(int page, int size);

    LessonDto addLesson(LessonDto lessonDto);

    LessonDto updateLesson(Long lessonId, LessonDto lessonDto);

    void deleteLesson(Long lessonId);
}
