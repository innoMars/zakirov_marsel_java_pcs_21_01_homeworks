package ru.pcs.ates.dto;


import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WrapperResponse {

    private Object response;

    private Long time;
}
