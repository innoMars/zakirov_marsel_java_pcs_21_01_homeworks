package ru.pcs.ates.dao;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.ates.models.Course;


public interface CoursesRepository extends JpaRepository<Course, Long> {

    Page<Course> findAllByIsDeletedIsNull(Pageable pageable);
}
