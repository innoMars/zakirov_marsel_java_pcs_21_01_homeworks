package ru.pcs.ates.services;


import ru.pcs.ates.dto.CourseDto;
import ru.pcs.ates.dto.LessonDto;
import ru.pcs.ates.models.Lesson;

import java.util.List;


public interface CoursesService {
    List<CourseDto> getCourses(int page, int size);

    CourseDto addCourse(CourseDto courseDto);

    CourseDto updateCourse(Long courseId, CourseDto courseDto);

    void deleteCourse(Long courseId);

    List<LessonDto> addLessonToCourse(Long courseId, Lesson lesson);

    List<LessonDto> deleteLessonInCourse(Long courseId, Lesson lesson);
}
