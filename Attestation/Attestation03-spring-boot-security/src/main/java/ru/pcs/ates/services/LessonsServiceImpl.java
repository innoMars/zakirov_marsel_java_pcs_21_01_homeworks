package ru.pcs.ates.services;


import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.pcs.ates.dao.LessonsRepository;
import ru.pcs.ates.dto.LessonDto;
import ru.pcs.ates.models.Lesson;


import static ru.pcs.ates.dto.LessonDto.from;

import java.util.List;

@Service
@RequiredArgsConstructor
public class LessonsServiceImpl implements LessonsService {

    private final LessonsRepository lessonsRepository;

    @Override
    public List<LessonDto> getLessons(int page, int size) {
        PageRequest request = PageRequest.of(page, size, Sort.by("id"));
        Page<Lesson> result = lessonsRepository.findAllByIsDeletedIsNull(request);
        return from(result.getContent());
    }

    @Override
    public LessonDto addLesson(LessonDto lessonDto) {
        Lesson newLesson = Lesson.builder()
                .name(lessonDto.getName())
                .build();
        lessonsRepository.save(newLesson);
        return from(newLesson);
    }

    @Override
    public LessonDto updateLesson(Long lessonId, LessonDto lessonDto) {
        Lesson lesson = lessonsRepository.getById(lessonId);
        lesson.setName(lessonDto.getName());
        lessonsRepository.save(lesson);

        return from(lesson);
    }

    @Override
    public void deleteLesson(Long lessonId) {
        Lesson lesson = lessonsRepository.getById(lessonId);
        lesson.setIsDeleted(true);
        lessonsRepository.save(lesson);
    }
}
