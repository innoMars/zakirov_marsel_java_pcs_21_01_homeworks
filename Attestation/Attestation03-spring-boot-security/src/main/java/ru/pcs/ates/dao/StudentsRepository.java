package ru.pcs.ates.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.ates.models.Student;


public interface StudentsRepository extends JpaRepository<Student, Long> {

    Page<Student> findAllByIsDeletedIsNull(Pageable pageable);
}
