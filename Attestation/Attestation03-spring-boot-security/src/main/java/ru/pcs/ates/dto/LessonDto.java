package ru.pcs.ates.dto;

import lombok.*;
import ru.pcs.ates.models.Lesson;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LessonDto {

    private Long id;

    private String name;

    public static LessonDto from(Lesson lesson) {
        return LessonDto.builder()
                .id(lesson.getId())
                .name(lesson.getName())
                .build();
    }

    public static List<LessonDto> from(List<Lesson> lessons) {
        return lessons.stream().map(LessonDto::from).collect(Collectors.toList());
    }
}
