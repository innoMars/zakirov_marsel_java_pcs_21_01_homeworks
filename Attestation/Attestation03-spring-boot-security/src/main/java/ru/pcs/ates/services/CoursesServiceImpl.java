package ru.pcs.ates.services;


import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.pcs.ates.dao.CoursesRepository;
import ru.pcs.ates.dao.LessonsRepository;
import ru.pcs.ates.dto.CourseDto;
import ru.pcs.ates.dto.LessonDto;
import ru.pcs.ates.models.Course;
import ru.pcs.ates.models.Lesson;

import java.util.List;

import static ru.pcs.ates.dto.CourseDto.from;
import static ru.pcs.ates.dto.LessonDto.from;

@Service
@RequiredArgsConstructor
public class CoursesServiceImpl implements CoursesService {

    private final CoursesRepository coursesRepository;
    private final LessonsRepository lessonRepository;

    @Override
    public List<CourseDto> getCourses(int page, int size) {
        PageRequest request = PageRequest.of(page, size, Sort.by("id"));
        Page<Course> result = coursesRepository.findAllByIsDeletedIsNull(request);

        return from(result.getContent());
    }

    @Override
    public CourseDto addCourse(CourseDto courseDto) {
        Course newCourse = Course.builder()
                .title(courseDto.getTitle())
                .build();
        coursesRepository.save(newCourse);
        return from(newCourse);
    }

    @Override
    public CourseDto updateCourse(Long courseId, CourseDto courseDto) {
        Course existedCourse = coursesRepository.getById(courseId);
        existedCourse.setTitle(courseDto.getTitle());
        coursesRepository.save(existedCourse);
        return from(existedCourse);
    }

    @Override
    public void deleteCourse(Long courseId) {
        Course course = coursesRepository.getById(courseId);
        course.setIsDeleted(true);
        coursesRepository.save(course);
    }

    @Override
    public List<LessonDto> addLessonToCourse(Long courseId, Lesson lesson) {
        Course course = coursesRepository.getById(courseId);
        Lesson existedLesson = lessonRepository.getById(lesson.getId());
        existedLesson.setCourse(course);
        course.getLessons().add(existedLesson);
        coursesRepository.save(course);
        lessonRepository.save(existedLesson);
        return from(course.getLessons());
    }

    @Override
    public List<LessonDto> deleteLessonInCourse(Long courseId, Lesson lesson) {
        Course course = coursesRepository.getById(courseId);
        Lesson existedLesson = lessonRepository.getById(lesson.getId());
        if (!course.getLessons().contains(existedLesson)) {
            return from(course.getLessons());
        }
        course.getLessons().remove(existedLesson);
        existedLesson.setCourse(null);
        coursesRepository.save(course);
        lessonRepository.save(existedLesson);
        return from(course.getLessons());
    }
}
