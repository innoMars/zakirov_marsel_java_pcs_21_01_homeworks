package ru.pcs.ates.dto;

import lombok.*;
import ru.pcs.ates.models.Student;


import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StudentDto {

    private Long id;
    private String firstName;
    private String lastName;

    public static StudentDto from(Student student) {
        return StudentDto.builder()
                .id(student.getId())
                .firstName(student.getFirstName())
                .lastName(student.getLastName())
                .build();
    }

    public static List<StudentDto> from(List<Student> students) {
        return students.stream().map(StudentDto::from).collect(Collectors.toList());
    }
}
