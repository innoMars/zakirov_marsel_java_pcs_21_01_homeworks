package ru.pcs.ates.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.ates.models.Lesson;


public interface LessonsRepository extends JpaRepository<Lesson,Long> {

    Page<Lesson> findAllByIsDeletedIsNull(Pageable pageable);
}
