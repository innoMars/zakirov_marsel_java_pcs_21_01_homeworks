package ru.pcs.ates.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SignInForm {

    private String password;

    private String email;
}
