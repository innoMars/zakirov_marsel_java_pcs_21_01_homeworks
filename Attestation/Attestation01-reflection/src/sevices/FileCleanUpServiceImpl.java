package sevices;


import com.sun.jdi.PrimitiveValue;

import javax.lang.model.type.PrimitiveType;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FileCleanUpServiceImpl implements FileCleanUpService {

    public void cleanup(Object object, Set<String> fieldsToCleanup, Set<String> fieldsToOutput) {

        if (Map.class.isAssignableFrom(object.getClass())) {
            mapDeleteAndSOUT((Map) object, fieldsToCleanup, fieldsToOutput);
        }
        else if (!Map.class.isAssignableFrom(object.getClass())){
            List<String> listFieldObject = new ArrayList<>();
            Field[] fieldsObject = object.getClass().getDeclaredFields();

            for (Field field : fieldsObject) {
                field.setAccessible(true);
                try {
                    listFieldObject.add(field.get(object).toString());
                } catch (IllegalAccessException e) {
                    throw new IllegalArgumentException(e);
                }
            }
                cleanUpObject(object, fieldsToCleanup, listFieldObject, fieldsObject);
                soutFieldToOutput(object, fieldsToOutput, fieldsObject, listFieldObject);

        }
    }

    private void mapDeleteAndSOUT(Map object, Set<String> fieldsToCleanup, Set<String> fieldsToOutput) {
        if (fieldsToCleanup.containsAll(object.keySet())) {
            //для списка fieldsToCleanup удалить ключи
            Field[] declaredFields = fieldsToCleanup.getClass().getDeclaredFields();
            for(Field field : declaredFields) {
                field.setAccessible(true);
                    object.keySet().removeAll(fieldsToCleanup);
            }
        } else {
            throw new IllegalArgumentException("Нет совпадений");
        }
        if (fieldsToOutput.containsAll(object.keySet())) {

//                    fieldsToOutput - вывести в консоль значения.
                    System.out.println(object.entrySet());
            } else {
            throw new IllegalArgumentException("Нет совпадений");
        }
    }

    private void cleanUpObject(Object object, Set<String> fieldsToCleanup, List<String> listFieldObject,
                               Field[] fieldsObject) {

        for (Field field : fieldsObject) {
            field.setAccessible(true);
            if (fieldsToCleanup.containsAll(listFieldObject)) {
                try {
                    if (field.getType().isPrimitive()) {
                        if (soryMeImTiredSoMuch(object, field)) continue;
                        field.set(object,0);
                    } else {
                        field.set(object, null);
                    }
                } catch (IllegalAccessException e) {
                   throw new IllegalArgumentException(e);
                }
            } else {
                throw new IllegalArgumentException("Нет совпадений");
            }
        }

    }

    private boolean soryMeImTiredSoMuch(Object object, Field field) throws IllegalAccessException {
        if(field.getType().equals(boolean.class)){
            field.set(object,false);
            return true;
        }
        if(field.getType().equals(char.class)){
            field.set(object,'\u0000');
            return true;
        }
        return false;
    }


    private void soutFieldToOutput(Object object, Set<String> fieldsToOutput,
                                   Field[] fieldsObject, List<String> listFieldObject) {

//        int count = 0;
        for (Field field : fieldsObject) {
            if (fieldsToOutput.containsAll(listFieldObject)) {
                field.setAccessible(true);
                if (field.getType().isPrimitive()) {
                    try {
                        //если вывести надо было именно то что совпало, то, String.valueOf(listFieldObject.get(count)
                        System.out.println(field.get((object)));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        //если вывести надо было именно то что совпало, то, (listFieldObject.get(count)).toString()
                        System.out.println(field.get(object));
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
//                count++;
            } else {
                throw new IllegalArgumentException("Нет совпадений");
            }
        }
    }

}
