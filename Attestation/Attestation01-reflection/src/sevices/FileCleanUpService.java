package sevices;

import java.util.Set;

public interface FileCleanUpService {
    void cleanup(Object object, Set<String> fieldsToCleanup, Set<String> fieldsToOutput);
}
