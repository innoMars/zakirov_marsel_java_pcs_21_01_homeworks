package entity;

public class Pokemon {
    String name;
    boolean java;
    String cool;

    public Pokemon(String name, boolean java, String cool) {
        this.name = name;
        this.java = java;
        this.cool = cool;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Pokemon{" +
                "name='" + name + '\'' +
                '}';
    }
}
