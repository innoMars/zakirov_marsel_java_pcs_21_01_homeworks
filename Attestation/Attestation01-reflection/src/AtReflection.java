import entity.Pokemon;
import sevices.FileCleanUpService;
import sevices.FileCleanUpServiceImpl;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AtReflection {
    public static void main(String[] args) {
        FileCleanUpService fileCleanUpService = new FileCleanUpServiceImpl();
        Set<String> fieldsToCleanup = new HashSet<>();
        Set<String> fieldsToOutput = new HashSet<>();
        Map<String, String> map = new HashMap<>();
        map.put("Monkey","Banana");
        map.put("Cat","Fish");
        Pokemon pokemon = new Pokemon("Pokemon",true,"Yes");

        fieldsToCleanup.add("5.0");
        fieldsToCleanup.add("Cat");
        fieldsToCleanup.add("Monkey");
        fieldsToCleanup.add("0");
        fieldsToCleanup.add("Yes");
        fieldsToCleanup.add("true");
        fieldsToCleanup.add("Pokemon");
        fieldsToCleanup.add("7");
        fieldsToOutput.add("Yes");
        fieldsToOutput.add("Banana");
        fieldsToOutput.add("Fish");
        fieldsToOutput.add("Monkey");
        fieldsToOutput.add("7");
        fieldsToOutput.add("Pokemon");
        fieldsToOutput.add("5.0");
        fieldsToOutput.add("true");
        fieldsToOutput.add("Сat");
        fileCleanUpService.cleanup(pokemon,fieldsToCleanup,fieldsToOutput);
//        fileCleanUpService.cleanup(pokemon2,fieldsToCleanup,fieldsToOutput);

    }
}
