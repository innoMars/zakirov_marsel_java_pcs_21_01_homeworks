package ru.pcs.web.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.pcs.web.validation.NotSameDescAndName;


import javax.validation.constraints.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@NotSameDescAndName
public class UploadProductForm {
    @NotBlank
    @Size(min = 3, max = 22)
    private String name;


    @DecimalMax("987654321.9")
    @DecimalMin("1.0")
    @NotNull
    private Double price;

    @NotBlank
    @Size(min = 3, max = 155)
    private String description;

}
