package ru.pcs.web.services;

import ru.pcs.web.dto.UploadProductForm;

public interface ProductService {
    void uploadProduct(UploadProductForm form);
}
