package ru.pcs.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.models.Product;

public interface ProductRepository extends JpaRepository<Product,Long> {
}
