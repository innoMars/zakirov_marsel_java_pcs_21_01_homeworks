package ru.pcs.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Homework30SpringBootMvcApplication {

    public static void main(String[] args) {
        SpringApplication.run(Homework30SpringBootMvcApplication.class, args);
    }

}
