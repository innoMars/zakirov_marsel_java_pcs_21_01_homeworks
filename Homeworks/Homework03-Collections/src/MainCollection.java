import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MainCollection {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите строку.");
        String inputLine = scanner.nextLine();
        char[] chars = inputLine.toCharArray();

        System.out.println(Arrays.toString(chars));

        Map<Character, Integer> mapValue = new HashMap<>();

        for (int i = 0; i < chars.length; i++) {
            if (mapValue.containsKey(chars[i])) {
                mapValue.put(chars[i], mapValue.get(chars[i]) + 1);
            } else {
                mapValue.put(chars[i], 1);
            }
        }
        for (Map.Entry<Character, Integer> m : mapValue.entrySet()) {
            System.out.println(m);
        }

    }
}
