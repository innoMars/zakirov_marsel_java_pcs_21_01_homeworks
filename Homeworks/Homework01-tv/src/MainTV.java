import entity.Channel;
import entity.Controller;
import entity.Program;
import entity.TV;

public class MainTV {
    public static void main(String[] args) {
        TV tv = new TV("Sony");
        Channel channel = new Channel("РЕН-ТВ");
        Program p1 = new Program("Кто управляет РЕН-ТВ?");
        Program p2 = new Program("Сатурн, причём здесь пельмени?");
        Program p3 = new Program("Вечерний Чубака");

        Controller controller = new Controller(tv);
        tv.addChannel(channel);

        channel.addProgram(p1);
        channel.addProgram(p2);
        channel.addProgram(p3);

        controller.on(1);
    }
}
