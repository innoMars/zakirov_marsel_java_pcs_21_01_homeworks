package entity;

import java.util.Random;

public class Controller {
    private TV tv;

    public Controller(TV tv) {
        this.tv = tv;
    }

    public void on(int channel) {
        Channel[] channels = tv.getChannels();

        Random random = new Random();
        System.out.println("Включен канал: " + channels[channel - 1].getName() + "\nИдёт передача: " +
                channels[channel - 1].getPrograms()[random.nextInt(3)].getName());

    }
}
