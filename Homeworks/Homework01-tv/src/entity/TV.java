package entity;

public class TV {
    private String model;
    private Channel[] channels;
    private int channelCount;

    public TV(String model) {
        this.model = model;
        this.channels = new Channel[3];
        this.channelCount = 0;
    }

    public String getModel() {
        return model;
    }

    public Channel[] getChannels() {
        return channels;
    }

    public void addChannel(Channel channel) {
        if (channelCount == channels.length) {
            System.err.println("Максимальное число каналов");
            return;
        }
        channels[channelCount] = channel;
        channelCount++;
    }
}
