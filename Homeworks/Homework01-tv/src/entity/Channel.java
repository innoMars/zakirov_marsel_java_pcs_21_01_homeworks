package entity;

public class Channel {
    private String name;
    private Program[] programs;
    private int programCount;

    public Channel(String name) {
        this.name = name;
        this.programs = new Program[3];
        this.programCount = 0;
    }

    public String getName() {
        return name;
    }

    public Program[] getPrograms() {
        return programs;
    }

    public void addProgram(Program program) {
        if (programCount == programs.length) {
            System.err.println("Максимальное число передач");
            return;
        }
        programs[programCount] = program;
        programCount++;
    }
}
