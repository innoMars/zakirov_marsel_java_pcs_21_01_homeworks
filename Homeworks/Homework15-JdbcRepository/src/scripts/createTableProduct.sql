create table product
(
    id         serial primary key,
    name       varchar(30),
    price      integer,
    booking_id integer
);

select setval('product_id_seq',20,true);