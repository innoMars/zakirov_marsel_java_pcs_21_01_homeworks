package app;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import entity.Product;
import repositories.ProductRepositories;
import repositories.ProductRepositoriesImpl;

public class JdbcRepository {
    public static final String ORG_POSTGESQL_DRIVER = "org.postgresql.Driver";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "123";
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/Product_test";

    public static void main(String[] args) {
        HikariConfig config = new HikariConfig();
        config.setUsername(DB_USER);
        config.setPassword(DB_PASSWORD);
        config.setDriverClassName(ORG_POSTGESQL_DRIVER);
        config.setJdbcUrl(DB_URL);


        HikariDataSource dataSource = new HikariDataSource(config);
        ProductRepositories productRepositories = new ProductRepositoriesImpl(dataSource);
        Product product = new Product("Meat", 300, 4);
        Product productDelete = new Product(14L, "Meat", 300, 4);
        Product productUpdate = new Product(3L, "Chips", 700, 99);
//        productRepositories.save(product);
//        productRepositories.update(productUpdate);
//        productRepositories.delete(productDelete);
//        productRepositories.deleteById(6L);
        System.out.println(productRepositories.findById(2L));
        System.out.println(productRepositories.findAll(0, 10));


    }
}
