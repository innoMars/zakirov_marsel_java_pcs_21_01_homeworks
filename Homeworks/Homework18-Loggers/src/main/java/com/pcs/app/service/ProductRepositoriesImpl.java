package com.pcs.app.service;

import com.pcs.app.entity.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class ProductRepositoriesImpl implements ProductRepositories {
    private static final Logger logger = LoggerFactory.getLogger(ProductRepositoriesImpl.class);
    //language=SQL
    private static final String SQL_SELECT_ALL =
            "select id, name, price, booking_id from product order by id limit ? offset ?";

    private static final Function<ResultSet, Product> productMapper = resultSet -> {
        try {
            Long id = resultSet.getLong("id");
            String name = resultSet.getString("name");
            Integer price = resultSet.getObject("price", Integer.class);
            Integer booking_id = resultSet.getObject("booking_id", Integer.class);
            return new Product(id, name, price, booking_id);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    };
    private final DataSource dataSource;


    public ProductRepositoriesImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Product> findAll(int page, int size) {
        logger.debug("select all with page = {}, size = {}", page, size);
        List<Product> products = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL)) {
            logger.debug("create connection");
            statement.setInt(1, size);
            statement.setInt(2, page * size);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    products.add(productMapper.apply(resultSet));
                }
                logger.debug("finish query");
                return products;
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }


}
