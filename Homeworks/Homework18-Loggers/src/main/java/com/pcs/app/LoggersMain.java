package com.pcs.app;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.pcs.app.entity.Clients;
import com.pcs.app.service.ProductRepositories;
import com.pcs.app.service.ProductRepositoriesImpl;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import config.Config;

@Parameters(separators = "=")
public class LoggersMain {
    @Parameter(names = {"--hikari-pool-size"})
    private int poolSize;

    public static void main(String[] args) {
        Config configFile = new Config();

        HikariConfig config = new HikariConfig();
        config.setUsername(configFile.getDbUser());
        config.setPassword(configFile.getDbPassword());
        config.setDriverClassName(configFile.getDbOrg());
        config.setJdbcUrl(configFile.getDbURL());
        config.setMaximumPoolSize(configFile.getThreadsCount());

        LoggersMain loggersMain = new LoggersMain();
        JCommander.newBuilder()
                .addObject(loggersMain)
                .build()
                .parse(args);
        if (loggersMain.poolSize != 0) {
            config.setMaximumPoolSize(loggersMain.poolSize);

        }


        HikariDataSource dataSource = new HikariDataSource(config);
        ProductRepositories productRepositories = new ProductRepositoriesImpl(dataSource);

        Clients clients = new Clients();
        for (int i = 0; i < configFile.getThreadsCount(); i++) {

            for (int j = 0; j < 10; j++) {
                final int finalI = j;
                clients.newClient(() ->
                        System.out.println(productRepositories.findAll(finalI, 10)));

            }
        }
        clients.run();
        clients.stop();

    }
}
