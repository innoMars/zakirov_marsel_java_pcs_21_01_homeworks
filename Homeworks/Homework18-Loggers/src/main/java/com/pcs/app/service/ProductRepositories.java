package com.pcs.app.service;

import com.pcs.app.entity.Product;

import java.util.List;

public interface ProductRepositories {
    List<Product> findAll(int page, int size);
}
