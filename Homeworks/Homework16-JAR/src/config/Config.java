package config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Config {
    private int threadsCount;
    private String dbUser;
    private String dbPassword;
    private String dbURL;
    private String dbOrg;

    public Config() {
        Properties properties = new Properties();
        try {
            properties.load(
                    new FileInputStream("Homeworks/Homework16-JAR/src/resources/threads.properties"));
            this.threadsCount = Integer.parseInt(properties.getProperty("executor.threads.count"));
            this.dbUser =properties.getProperty("db.user");
            this.dbPassword = properties.getProperty("db.password");
            this.dbURL = properties.getProperty("db.URL");
            this.dbOrg = properties.getProperty("db.org");
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public int getThreadsCount() {
        return threadsCount;
    }

    public String getDbUser() {
        return dbUser;
    }

    public String getDbPassword() {
        return dbPassword;
    }

    public String getDbURL() {
        return dbURL;
    }

    public String getDbOrg() {
        return dbOrg;
    }
}
