package app;

import app.service.ProductRepositories2;
import app.service.ProductRepositories2Impl;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import config.Config;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@Parameters(separators = "=")
public class JAR {
    @Parameter(names = {"--hikari-pool-size"})
    private int poolSize;

    public static void main(String[] args) {
        Config configFile = new Config();

        HikariConfig config = new HikariConfig();
        config.setUsername(configFile.getDbUser());
        config.setPassword(configFile.getDbPassword());
        config.setDriverClassName(configFile.getDbOrg());
        config.setJdbcUrl(configFile.getDbURL());


        JAR jar = new JAR();
        JCommander.newBuilder()
                .addObject(jar)
                .build()
                .parse(args);
        if (jar.poolSize != 0) {
            config.setMaximumPoolSize(jar.poolSize);
        }

        HikariDataSource dataSource = new HikariDataSource(config);
        ProductRepositories2 productRepositories = new ProductRepositories2Impl(dataSource);
        ExecutorService service = Executors.newFixedThreadPool(configFile.getThreadsCount());

        for (int i = 0; i < configFile.getThreadsCount(); i++) {
            service.submit(() -> {
                for (int j = 0; j < 1000; j++) {
                    try {
                        System.out.println(productRepositories.findAll(j, 10));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        service.shutdown();


    }
}
