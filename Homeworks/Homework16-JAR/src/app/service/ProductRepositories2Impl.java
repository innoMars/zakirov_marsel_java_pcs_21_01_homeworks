package app.service;

import javax.sql.DataSource;
import app.entity.Product2;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class ProductRepositories2Impl implements ProductRepositories2 {
    //language=SQL
    private static final String SQL_SELECT_ALL =
            "select id, name, price, booking_id from product order by id limit ? offset ?";

    private static final Function<ResultSet, Product2> productMapper = resultSet -> {
        try {
            Long id = resultSet.getLong("id");
            String name = resultSet.getString("name");
            Integer price = resultSet.getObject("price", Integer.class);
            Integer booking_id = resultSet.getObject("booking_id", Integer.class);
            return new Product2(id, name, price, booking_id);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    };
    private final DataSource dataSource;


    public ProductRepositories2Impl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Product2> findAll(int page, int size) {
        List<Product2> students = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL)) {
            statement.setInt(1, size);
            statement.setInt(2, page * size);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    students.add(productMapper.apply(resultSet));
                }
                return students;
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }


}
