package app.service;

import app.entity.Product2;
import java.util.List;

public interface ProductRepositories2 {

    List<Product2> findAll(int page, int size);

}
