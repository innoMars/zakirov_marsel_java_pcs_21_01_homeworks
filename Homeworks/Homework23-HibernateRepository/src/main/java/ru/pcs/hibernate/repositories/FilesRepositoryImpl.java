package ru.pcs.hibernate.repositories;

import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.pcs.hibernate.models.FileDto;


import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


public class FilesRepositoryImpl implements FilesRepository {


    @Override
    public void save(String filePath) {
        SessionFactory sessionFactory = getSessionFactory();
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Path path = Paths.get(filePath);
        FileDto fileDto = FileDto.builder()
                .originalFileName(String.valueOf(path.getFileName()))
                .storageFileName(String.valueOf(UUID.randomUUID()))
                .size(path.toFile().length())
                .mimeType(String.valueOf(UUID.randomUUID()))
                .description("Desc")
                .build();
        session.persist(fileDto);
        session.getTransaction().commit();
        session.close();
        sessionFactory.close();
    }


    @Override
    public Optional<FileDto> findByOrigName(String origName) {
        SessionFactory sessionFactory = getSessionFactory();
        Session session = sessionFactory.openSession();

        Query fileDtoQuery = session.createQuery("select filedto from FileDto filedto" +
                " where original_file_name = :original_file_name", FileDto.class).setParameter("original_file_name", origName);
        List<FileDto> fileDtoList = fileDtoQuery.getResultList();
        session.close();
        sessionFactory.close();
        return fileDtoList.isEmpty() ? Optional.empty() : Optional.of(fileDtoList.get(0));
    }

    private SessionFactory getSessionFactory() {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");
        return configuration.buildSessionFactory();
    }
}
