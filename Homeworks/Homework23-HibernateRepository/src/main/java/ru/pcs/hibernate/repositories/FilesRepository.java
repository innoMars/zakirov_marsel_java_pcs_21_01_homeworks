package ru.pcs.hibernate.repositories;

import ru.pcs.hibernate.models.FileDto;

import java.util.Optional;

public interface FilesRepository {
    void save(String filePath);
    Optional<FileDto> findByOrigName(String origName);
}
