<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Accounts page</title>
</head>
<body>
<h1 style="color:${color}">Accounts size - ${accounts.size()} </h1>
<table>
    <tr>
        <th>ID</th>
        <th>First Name</th>
        <th>Last Name</th>
    </tr>
    <c:forEach items="${accounts}" var="account">
    <tr>
        <td>${account.id}</td>
        <td>${account.firstName}</td>
        <td>${account.lastName}</td>
    </tr>
    </c:forEach>
</table>
</body>
</html>
