
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Profile</title>
</head>
<body>
<h1 style="color:${color}">Profile</h1>
<h1>First Name: ${account.firstName}</h1>
<h1>Last Name: ${account.lastName}</h1>
<form action="/logout" method="post">
    <input type="submit" value="Logout">
</form>
</body>
</html>
