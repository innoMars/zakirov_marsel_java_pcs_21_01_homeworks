package ru.pcs.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.pcs.entity.Account;

import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AccountDto {
    private Long id;
    private String firstName;
    private String lastName;

    public static AccountDto from(Account account){
        return AccountDto.builder()
                .id(account.getId())
                .firstName(account.getFirstName())
                .lastName(account.getLastName())
                .build();
    }

    public  static List<AccountDto> from(List<Account> accounts){
        return accounts.stream().map(AccountDto::from).collect(Collectors.toList());
    }
}
