package ru.pcs.services;

import ru.pcs.dto.AccountDto;
import ru.pcs.entity.Account;
import ru.pcs.repositories.AccountsRepository;

import java.util.List;

import static ru.pcs.dto.AccountDto.from;


public class AccountsServiceImpl implements AccountsService {
    AccountsRepository accountsRepository;

    public AccountsServiceImpl(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    @Override
    public List<AccountDto> getAll() {
        return  from(accountsRepository.findAll());
    }
}
