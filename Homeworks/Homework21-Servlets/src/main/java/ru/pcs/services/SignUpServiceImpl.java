package ru.pcs.services;

import ru.pcs.dto.SignUpForm;
import ru.pcs.entity.Account;
import ru.pcs.repositories.AccountsRepository;

import java.util.Locale;

public class SignUpServiceImpl implements SignUpService {
    private final AccountsRepository accountsRepository;

    public SignUpServiceImpl(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    @Override
    public void signUp(SignUpForm signUpForm) {
        Account account = Account.builder()
                .firstName(signUpForm.getFirstName())
                .lastName(signUpForm.getLastName())
                .email(signUpForm.getEmail().toLowerCase(Locale.ROOT))
                .password(signUpForm.getPassword())
                .build();

        accountsRepository.save(account);
    }
}
