package ru.pcs.services;

import ru.pcs.dto.AccountDto;
import ru.pcs.entity.Account;
import ru.pcs.repositories.AccountsRepository;

import java.util.Optional;


public class ProfileServiceImpl implements ProfileService {
    private AccountsRepository accountsRepository;

    public ProfileServiceImpl(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    @Override
    public AccountDto getAc(String email) {
        Optional<Account> account = accountsRepository.findByEmail(email);
        return AccountDto.builder()
                .firstName(account.get().getFirstName())
                .lastName(account.get().getLastName())
                .build();
    }
}
