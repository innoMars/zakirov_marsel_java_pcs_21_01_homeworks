package ru.pcs.services;

import ru.pcs.dto.SignUpForm;

public interface SignUpService {
    void signUp(SignUpForm signUpForm);
}
