package ru.pcs.services;

import ru.pcs.dto.AccountDto;
import ru.pcs.entity.Account;

import java.util.List;

public interface AccountsService {
    List<AccountDto> getAll();
}
