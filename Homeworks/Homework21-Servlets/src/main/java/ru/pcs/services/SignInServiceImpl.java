package ru.pcs.services;

import ru.pcs.dto.SignInForm;
import ru.pcs.entity.Account;
import ru.pcs.repositories.AccountsRepository;

import java.util.Optional;

public class SignInServiceImpl implements SignInService {
    private final AccountsRepository accountsRepository;

    public SignInServiceImpl(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    @Override
    public boolean doAuthenticated(SignInForm signInForm) {
        Optional<Account> accountOptional = accountsRepository.findByEmail(signInForm.getEmail());

        return accountOptional.map(account -> account.getPassword()
                .equals(signInForm.getPassword()))
                .orElse(false);
    }
}
