package ru.pcs.services;

import ru.pcs.dto.SignInForm;

public interface SignInService {
    boolean doAuthenticated(SignInForm signInForm);
}
