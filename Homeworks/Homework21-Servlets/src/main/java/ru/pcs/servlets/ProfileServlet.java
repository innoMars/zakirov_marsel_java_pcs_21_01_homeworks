package ru.pcs.servlets;

import ru.pcs.dto.AccountDto;
import ru.pcs.entity.Account;
import ru.pcs.filters.ColorFilter;
import ru.pcs.repositories.AccountsRepository;
import ru.pcs.repositories.AccountsRepositoryImpl;
import ru.pcs.services.AccountsServiceImpl;
import ru.pcs.services.ProfileService;
import ru.pcs.services.ProfileServiceImpl;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@WebServlet("/profile")
public class ProfileServlet extends HttpServlet {
    private ProfileService profileService;
    private static final int COLOR_COOKIE_MAX_AGE = 60*60*24*365;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        DataSource dataSource = (DataSource) servletContext.getAttribute("dataSource");
        AccountsRepository accountsRepository = new AccountsRepositoryImpl(dataSource);
        this.profileService = new ProfileServiceImpl(accountsRepository);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pageColor = req.getParameter("color");
        if (pageColor != null) {
            req.setAttribute("color", pageColor);
            Cookie cookie = new Cookie(ColorFilter.COLOR_COOKIE_NAME, pageColor);
            cookie.setMaxAge(COLOR_COOKIE_MAX_AGE);
            resp.addCookie(cookie);
        }

        String email = (String) req.getSession(true).getAttribute("email");
        AccountDto accountDto = profileService.getAc(email);
        req.getSession(true).setAttribute("account",accountDto);


        req.getRequestDispatcher("jsp/profile.jsp").forward(req, resp);
    }

}
