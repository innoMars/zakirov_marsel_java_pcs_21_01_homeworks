package ru.pcs.servlets;

import ru.pcs.dto.SignInForm;
import ru.pcs.filters.AuthenticationFilter;
import ru.pcs.repositories.AccountsRepository;
import ru.pcs.repositories.AccountsRepositoryImpl;
import ru.pcs.services.SignInService;
import ru.pcs.services.SignInServiceImpl;
import ru.pcs.services.SignUpServiceImpl;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.io.IOException;

@WebServlet("/signIn")
public class SignInServlet extends HttpServlet {

    SignInService signInService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("jsp/signIn.jsp").forward(req,resp);
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        DataSource dataSource = (DataSource) servletContext.getAttribute("dataSource");
        AccountsRepository accountsRepository = new AccountsRepositoryImpl(dataSource);
        this.signInService = new SignInServiceImpl(accountsRepository);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        SignInForm signInForm = SignInForm.builder()
                .email(req.getParameter("email"))
                .password(req.getParameter("password"))
                .build();
        if (signInService.doAuthenticated(signInForm)){
            HttpSession session = req.getSession(true);
            session.setAttribute(AuthenticationFilter.DEFAULT_AUTHENTICATED_ATTR_NAME,true);
            resp.sendRedirect("/profile");
            req.getSession(true).setAttribute("email",signInForm.getEmail());
        }else {
            resp.sendRedirect("/signIn?error");
        }

    }
}
