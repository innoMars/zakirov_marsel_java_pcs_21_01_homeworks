package ru.pcs.servlets;

import ru.pcs.dto.AccountDto;
import ru.pcs.repositories.AccountsRepository;
import ru.pcs.repositories.AccountsRepositoryImpl;
import ru.pcs.services.AccountsService;
import ru.pcs.services.AccountsServiceImpl;
import ru.pcs.services.SignUpServiceImpl;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.List;

@WebServlet("/accounts")
public class AccountsServlet extends HttpServlet {
    private AccountsService accountsService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        DataSource dataSource = (DataSource) servletContext.getAttribute("dataSource");
        AccountsRepository accountsRepository = new AccountsRepositoryImpl(dataSource);
        this.accountsService = new AccountsServiceImpl(accountsRepository);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<AccountDto> accounts = accountsService.getAll();
        req.setAttribute("accounts",accounts);
        req.getRequestDispatcher("jsp/accounts.jsp").forward(req,resp);
    }
}
