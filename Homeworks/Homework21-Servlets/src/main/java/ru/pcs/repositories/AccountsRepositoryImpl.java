package ru.pcs.repositories;

import ru.pcs.entity.Account;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class AccountsRepositoryImpl implements AccountsRepository {
    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from account order by id";

    //language=SQL
    private static final String SQL_INSERT = "insert into account(first_name," +
            "last_name,email,password) values (?,?,?,?)";

    //language=SQL
    private static final String SQL_FIND_BY_EMAIL = "select * from account where email =?";

    private final DataSource dataSource;

    private static final Function<ResultSet, Account> accountMapper = row -> {
        try {
            return Account.builder()
                    .id(row.getLong("id"))
                    .firstName(row.getString("first_name"))
                    .lastName(row.getString("last_name"))
                    .email(row.getString("email"))
                    .password(row.getString("password"))
                    .build();
        } catch (SQLException throwables) {
            throw new IllegalArgumentException(throwables);
        }
    };

    public AccountsRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Account account) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {

            statement.setString(1, account.getFirstName());
            statement.setString(2, account.getLastName());
            statement.setString(4, account.getPassword());
            statement.setString(3, account.getEmail());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert account");
            }

            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                account.setId(generatedKeys.getLong("id"));
            } else {
                throw new SQLException("Can't get id");
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public List<Account> findAll() {
        List<Account> accounts = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    accounts.add(accountMapper.apply(resultSet));
                }
            }
            return accounts;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional<Account> findByEmail(String email) {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_EMAIL)) {
            statement.setString(1,email);

            try (ResultSet resultSet = statement.executeQuery() ){
                if (resultSet.next()){
                    return Optional.of(accountMapper.apply(resultSet));
                }
                return Optional.empty();
            }
        }catch (SQLException e){
            throw new IllegalArgumentException(e);
        }
    }
}
