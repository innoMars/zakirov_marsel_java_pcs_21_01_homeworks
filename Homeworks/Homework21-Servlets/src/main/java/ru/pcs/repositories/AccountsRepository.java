package ru.pcs.repositories;

import ru.pcs.entity.Account;

import java.util.List;
import java.util.Optional;

public interface AccountsRepository {
    void save(Account account);

    List<Account> findAll();

    Optional<Account> findByEmail(String email);
}
