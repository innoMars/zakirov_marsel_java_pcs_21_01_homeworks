package ru.pcs.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/*")
public class LoggerFilter implements Filter {

    private static final Logger LOGGER = LoggerFactory.getLogger(Logger.class.getName());

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest servletRequest1 = (HttpServletRequest) servletRequest;
        HttpServletResponse servletResponse1 = (HttpServletResponse) servletResponse;
        LOGGER.info(servletRequest1.getMethod() + " " + servletRequest1.getRequestURI());
        filterChain.doFilter(servletRequest1, servletResponse1);
    }

    @Override
    public void destroy() {

    }
}
