package ru.pcs.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@WebFilter("/*")
public class AuthenticationFilter implements Filter {
    public static final List<String> PROTECTED_URIS = Arrays.asList("/profile", "/accounts");

    private static final List<String> NOT_ACCESSED_AFTER_AUTHENTICATION = Arrays.asList("signIn", "signUp");

    private static final String DEFAULT_REDIRECT_URI = "/profile";

    private static final String DEFAULT_SIGN_IN_URI = "/signIn";

    public static final String DEFAULT_AUTHENTICATED_ATTR_NAME = "isAuthenticated";


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest servletRequest1 = (HttpServletRequest) servletRequest;
        HttpServletResponse servletResponse1 = (HttpServletResponse) servletResponse;
        if (isProtected(servletRequest1)) {
            if (isAuthenticated(servletRequest1)) {
                filterChain.doFilter(servletRequest1, servletResponse1);
            } else {
                servletResponse1.sendRedirect(DEFAULT_SIGN_IN_URI);
            }
            return;
        }
        if (isAuthenticated(servletRequest1) && NOT_ACCESSED_AFTER_AUTHENTICATION
                .contains(servletRequest1.getRequestURI())) {
            servletResponse1.sendRedirect(DEFAULT_REDIRECT_URI);
            return;
        }
        filterChain.doFilter(servletRequest1, servletResponse1);
    }

    private boolean isProtected(HttpServletRequest servletRequest1) {
        return PROTECTED_URIS.contains(servletRequest1.getRequestURI());
    }

    private boolean isAuthenticated(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return false;
        }
        Boolean result = (Boolean) session.getAttribute(DEFAULT_AUTHENTICATED_ATTR_NAME);
        return result != null && result;
    }

    @Override
    public void destroy() {

    }
}
