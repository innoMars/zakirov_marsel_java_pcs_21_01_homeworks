package ru.pcs.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/*")
public class ColorFilter implements Filter {

    public static  final String COLOR_COOKIE_NAME = "pageColor";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest servletRequest1 =(HttpServletRequest) servletRequest;
        HttpServletResponse servletResponse1 = (HttpServletResponse) servletResponse;
        Cookie[] cookies = servletRequest1.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(COLOR_COOKIE_NAME)) {
                    servletRequest1.setAttribute("color", cookie.getValue());
                }
            }
        }
        filterChain.doFilter(servletRequest1, servletResponse1);
    }

    @Override
    public void destroy() {

    }
}
