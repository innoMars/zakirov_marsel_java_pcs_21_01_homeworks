import entity.User;
import service.UserService;
import service.IdGenerator;
import service.IdGeneratorImpl;
import service.UserRepository;
import service.UserRepositoryImpl;

public class MainDAO {
    public static void main(String[] args) {
        IdGenerator idGenerator = new IdGeneratorImpl("Homeworks/Homework06-Repository/usersId.txt") ;
        UserRepository userRepository = new UserRepositoryImpl(
                "Homeworks/Homework06-Repository/users.txt",idGenerator);
        UserService userService = new UserService(userRepository);

//        Scanner scanner = new Scanner(System.in);
//        System.out.println(userRepository.count());
//        int i = 0;
//        while (i!=3){
//            String email = scanner.nextLine();
//            String password = scanner.nextLine();
//
//            userService.signUp(email,password);
//            i++;
//        }

        System.out.println(userRepository.findAll());

        System.out.println(userRepository.existsByEmail("email2@"));

        System.out.println(userRepository.count());
        User user = new  User(3,"email22@","12345678");

//        userRepository.delete(user);
		userRepository.update(user);
//        System.out.println(userRepository.findAll());
    }

}
