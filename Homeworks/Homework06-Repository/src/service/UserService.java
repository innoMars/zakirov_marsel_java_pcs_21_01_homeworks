package service;

import entity.User;
import exceptions.BadEmailException;
import exceptions.BadPasswordException;
import exceptions.UserNotFoundException;
import service.UserRepository;

import java.util.ArrayList;
import java.util.List;

public class UserService {

    private final UserRepository userRepository;
    private final List<User> userList = new ArrayList<>();

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void signUp(String email, String password) {
        User user = new User();
        if (!userRepository.existsByEmail(email)) {
            if (!email.contains("@")) {
                throw new BadEmailException("емейл пользователя должен содержать символ @");
            }
            user.setEmail(email);

            if (password.length() < 7 && password.contains(" ")) {
                throw new BadPasswordException("пароль пользователя должен состоять из букв и цифр и спец. символов," +
                        " длина > 7");
            }
            user.setPassword(password);

            userRepository.save(user);
        } else throw new IllegalArgumentException("Email already exists");
    }


    public void signIn(String email, String password) {

        User user = new User(email,password);
        if (!userRepository.existsByEmail(email)){
            throw new UserNotFoundException("Неверный пользователь или пароль");
        }
        if (userRepository.findByEmail(email).get().getPassword().equals(user.getPassword())){
            System.out.println("Вы авторизовались под "+user.getEmail());
        }else {
            throw new UserNotFoundException("Неверный пользователь или пароль");
        }

    }

}

