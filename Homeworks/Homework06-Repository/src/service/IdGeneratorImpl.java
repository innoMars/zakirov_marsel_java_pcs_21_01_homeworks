package service;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class IdGeneratorImpl implements IdGenerator {
    private final String fileName;

    public IdGeneratorImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public boolean hasNext() {
        return true;
    }

    @Override
    public Integer next() {
        try (Scanner scanner = new Scanner(new FileInputStream(fileName));) {
            int lastId = scanner.nextInt();
            lastId++;
            try (PrintWriter printWriter = new PrintWriter(new FileWriter(fileName))) {
                printWriter.print(lastId);
            }
            return lastId;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
