package service;

import entity.User;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class UserRepositoryImpl implements UserRepository {
    private final String fileName;
    private final IdGenerator idGenerator;


    public UserRepositoryImpl(String fileName, IdGenerator idGenerator) {
        this.fileName = fileName;
        this.idGenerator = idGenerator;
    }

    @Override
    public void update(User user) {
        List<User> users = null;
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            users = stream
                    .map(User::new)
                    .filter(userFil -> !userFil.getId().equals(user.getId()))
                    .collect(Collectors.toList());
            users.add(user);

            try (BufferedWriter writer = Files.newBufferedWriter(Paths.get("Homeworks/Homework06-Repository/usersNew.txt"))) {
                for (int i = 0; i < users.size(); i++) {
                    String userAsLine2 = users.get(i).getId() + " " + users.get(i).getEmail() + " " +
                            users.get(i).getPassword();
                    writer.write(userAsLine2);
                    writer.newLine();
                }
            }
            Files.copy(Paths.get("Homeworks/Homework06-Repository/usersNew.txt"),
                    Paths.get("Homeworks/Homework06-Repository/users.txt"), StandardCopyOption.REPLACE_EXISTING);
            Files.delete(Paths.get("Homeworks/Homework06-Repository/usersNew.txt"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional<User> findByEmail(String email) {
        Optional<User> result = null;
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            result = stream
                    .map(User::new)
                    .filter(user -> user.getEmail().equals(email))
                    .findFirst();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return result;
    }

    @Override
    public List<User> findAll() {
        List<User> users = null;
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            users = stream
                    .map(User::new)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return users;
    }

    @Override
    public void delete(User user) {

        List<User> users = null;

        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            users = stream
                    .map(User::new)
                    .filter(userFil -> !userFil.getId().equals(user.getId()))
                    .collect(Collectors.toList());

            try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(
                    "Homeworks/Homework06-Repository/usersNew.txt"))) {
                for (int i = 0; i < users.size(); i++) {
                    String userAsLine2 = users.get(i).getId() + " " + users.get(i).getEmail() + " " +
                            users.get(i).getPassword();
                    writer.write(userAsLine2);
                    writer.newLine();
                }
            }

            Files.copy(Paths.get("Homeworks/Homework06-Repository/usersNew.txt"), Paths.get(fileName),
                    StandardCopyOption.REPLACE_EXISTING);
            Files.delete(Paths.get("Homeworks/Homework06-Repository/usersNew.txt"));


        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public boolean existsByEmail(String email) {
        if (findByEmail(email).isPresent()) {
            return true;
        }
        return false;
    }

    @Override
    public void save(User user) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true))) {
            user.setId(idGenerator.next());
            String userAsLine = user.getId() + " " + user.getEmail() + " " + user.getPassword();
            writer.write(userAsLine);
            writer.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public int count() {
        return findAll().size();
    }


}
