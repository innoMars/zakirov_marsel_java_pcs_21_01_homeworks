package ru.pcs.web.services;

import ru.pcs.web.dto.AccountDto;
import ru.pcs.web.models.Account;
import ru.pcs.web.repositories.AccountsRepository;

import java.util.Optional;

public class ProfileServiceImpl implements ProfileService {

    private AccountsRepository accountsRepository;

    public ProfileServiceImpl(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    @Override
    public AccountDto getAccount(String email) {
        Optional<Account> account = accountsRepository.findByEmail(email);

        return AccountDto.builder()
            .id(account.get().getId())
            .firstName(account.get().getFirstName())
            .lastName(account.get().getLastName())
            .email(account.get().getEmail())
            .build();
    }
}
