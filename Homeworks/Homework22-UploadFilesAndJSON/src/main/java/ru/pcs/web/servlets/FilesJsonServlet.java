package ru.pcs.web.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.pcs.web.dto.AccountDto;
import ru.pcs.web.dto.FileDto;
import ru.pcs.web.models.Account;
import ru.pcs.web.repositories.FileInfoRepository;
import ru.pcs.web.repositories.FileInfoRepositoryJdbcImpl;
import ru.pcs.web.services.FilesService;
import ru.pcs.web.services.FilesServiceImpl;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

@WebServlet("/files/json")
public class FilesJsonServlet extends HttpServlet {

    private FilesService filesService;
    private ObjectMapper objectMapper;


    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        DataSource dataSource = (DataSource) servletContext.getAttribute("dataSource");
        FileInfoRepository fileInfoRepository = new FileInfoRepositoryJdbcImpl(dataSource);
        this.filesService = new FilesServiceImpl(fileInfoRepository);
        this.filesService.setStoragePath((String) servletContext.getAttribute("storagePath"));
        this.objectMapper = new ObjectMapper();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        AccountDto accountDto = (AccountDto) request.getSession(true).getAttribute("profile");


        List<FileDto> namesFiles = filesService.searchByOrig( request.getParameter("fileName")
                , accountDto.getId());

        response.setContentType("application/json");
        String json = objectMapper.writeValueAsString(namesFiles);
        response.getWriter().println(json);
    }


}
