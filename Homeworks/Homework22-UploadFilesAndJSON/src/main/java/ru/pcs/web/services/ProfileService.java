package ru.pcs.web.services;

import ru.pcs.web.dto.AccountDto;

public interface ProfileService {

    AccountDto getAccount(String email);
}
