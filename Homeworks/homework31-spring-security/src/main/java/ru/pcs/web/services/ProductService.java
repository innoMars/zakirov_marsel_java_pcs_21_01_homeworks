package ru.pcs.web.services;


import ru.pcs.web.dto.UploadProductForm;

import java.util.List;

public interface ProductService {
    void uploadProduct(UploadProductForm form);

    List<UploadProductForm> getAllProducts();

    void deleteProduct(Long productId);
}
