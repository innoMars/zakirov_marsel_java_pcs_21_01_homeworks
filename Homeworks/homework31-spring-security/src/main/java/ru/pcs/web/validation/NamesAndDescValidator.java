package ru.pcs.web.validation;

import ru.pcs.web.dto.UploadProductForm;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NamesAndDescValidator implements ConstraintValidator<NotSameDescAndName, UploadProductForm> {

    @Override
    public boolean isValid(UploadProductForm object, ConstraintValidatorContext context) {
        return !object.getName().equals(object.getDescription());
    }
}
