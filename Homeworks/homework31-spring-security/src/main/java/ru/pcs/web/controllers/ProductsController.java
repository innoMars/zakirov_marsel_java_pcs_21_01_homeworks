package ru.pcs.web.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import ru.pcs.web.services.ProductService;

@RequiredArgsConstructor
@Controller
@RequestMapping("/products")
public class ProductsController {

    private final ProductService productService;

    @GetMapping
    public String getProductsPage(Model model) {
        model.addAttribute("products", productService.getAllProducts());
        return "products";
    }

    @PostMapping("/{product-id}/delete")
    public String deleteAccount(@PathVariable("product-id") Long productId) {
        productService.deleteProduct(productId);
        return "redirect:/products";
    }
}
