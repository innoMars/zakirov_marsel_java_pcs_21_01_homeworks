package ru.pcs.web.models;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public enum State {
        NOT_CONFIRMED, CONFIRMED, DELETED
    };

    @Enumerated(value = EnumType.STRING)
    private Product.State state;

    private String name;
    private Double price;
    private String description;
}
