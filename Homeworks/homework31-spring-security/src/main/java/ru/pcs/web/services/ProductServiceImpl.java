package ru.pcs.web.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pcs.web.dto.UploadProductForm;
import ru.pcs.web.models.Product;
import ru.pcs.web.repositories.ProductRepository;

import java.util.List;
import static ru.pcs.web.dto.UploadProductForm.from;
@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Override
    public void uploadProduct(UploadProductForm form) {
        Product product = Product.builder()
                .name(form.getName())
                .price(form.getPrice())
                .description(form.getDescription())
                .build();

        productRepository.save(product);
    }

    @Override
    public List<UploadProductForm> getAllProducts() {
        return from(productRepository.findAllByState(Product.State.CONFIRMED));
    }

    @Override
    public void deleteProduct(Long productId) {
        Product product = productRepository.getById(productId);
        product.setState(Product.State.DELETED);
        productRepository.save(product);
    }

}
