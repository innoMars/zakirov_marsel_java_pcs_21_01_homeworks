package ru.pcs.web.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.pcs.web.models.Account;
import ru.pcs.web.models.Product;
import ru.pcs.web.validation.NotSameDescAndName;


import javax.validation.constraints.*;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@NotSameDescAndName
public class UploadProductForm {
    private Long id;
    @NotBlank
    @Size(min = 3, max = 22)
    private String name;


    @DecimalMax("987654321.9")
    @DecimalMin("1.0")
    private Double price;

    @NotBlank
    @Size(min = 3, max = 155)
    private String description;

    public static UploadProductForm from(Product product) {
        return UploadProductForm.builder()
                .id(product.getId())
                .name(product.getName())
                .description(product.getDescription())
                .price(product.getPrice())
                .build();
    }

    public static List<UploadProductForm> from(List<Product> products) {
        return products.stream().map(UploadProductForm::from).collect(Collectors.toList());
    }
}
