package ru.pcs.web.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.pcs.web.dto.UploadProductForm;
import ru.pcs.web.services.ProductService;

import javax.validation.Valid;

@RequiredArgsConstructor
@Controller
@RequestMapping("/uploadProduct")
public class UploadProductController {

    private final ProductService productService;

    @RequestMapping()
    public String getSignUpPage(Model model) {
        model.addAttribute("uploadProductForm", new UploadProductForm());
        return "uploadProduct";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String signUp(@Valid UploadProductForm form, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("uploadProductForm", form);
            return "uploadProduct";
        }
        productService.uploadProduct(form);
        return "redirect:/uploadProduct";
    }
}
