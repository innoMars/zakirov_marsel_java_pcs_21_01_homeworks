package ru.pcs.web.services;

import ru.pcs.web.dto.CourseDto;
import ru.pcs.web.dto.LessonDto;
import ru.pcs.web.models.Lesson;

import java.util.List;


public interface CoursesService {
    List<CourseDto> getCourses(int page, int size);

    CourseDto addCourse(CourseDto courseDto);

    CourseDto updateCourse(Long courseId, CourseDto courseDto);

    void deleteCourse(Long courseId);

    List<LessonDto> addLessonToCourse(Long courseId, Lesson lesson);

    List<LessonDto> deleteLessonInCourse(Long courseId, Lesson lesson);
}
