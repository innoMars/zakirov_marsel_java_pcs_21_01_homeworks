package ru.pcs.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.pcs.web.models.Method;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MethodDto {
    private String name;
    private Long countCalls;

    public static MethodDto from(Method method){
        return MethodDto.builder()
                .name(method.getName())
                .countCalls(method.getCountCalls())
                .build();
    }
}
