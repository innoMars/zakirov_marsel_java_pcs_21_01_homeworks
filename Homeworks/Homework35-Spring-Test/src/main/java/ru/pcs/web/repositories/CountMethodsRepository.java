package ru.pcs.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.models.Method;

public interface CountMethodsRepository extends JpaRepository<Method,String> {

}
