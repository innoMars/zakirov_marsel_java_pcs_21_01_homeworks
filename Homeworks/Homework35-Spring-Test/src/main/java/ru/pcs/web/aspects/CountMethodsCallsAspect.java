package ru.pcs.web.aspects;

import lombok.RequiredArgsConstructor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import ru.pcs.web.dto.MethodDto;
import ru.pcs.web.dto.StudentsResponse;
import ru.pcs.web.dto.WrapperResponse;
import ru.pcs.web.services.CountMethodsService;

@Component
@Aspect
@RequiredArgsConstructor
public class CountMethodsCallsAspect {


    private final CountMethodsService countMethodsService;

    @Around(value = "execution(* ru.pcs.web.controllers.*.*(..))")
    public ResponseEntity countMethodCalls(ProceedingJoinPoint joinPoint) throws Throwable {

        MethodDto methodDto = MethodDto.builder()
                .name(joinPoint.getSignature().getName())
                .build();

        countMethodsService.addCallMethod(methodDto);
        return (ResponseEntity) joinPoint.proceed();
    }
}
