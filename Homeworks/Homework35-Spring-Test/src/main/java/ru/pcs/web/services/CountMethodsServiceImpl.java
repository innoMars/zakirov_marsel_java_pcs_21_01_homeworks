package ru.pcs.web.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pcs.web.dto.MethodDto;
import ru.pcs.web.models.Method;
import ru.pcs.web.repositories.CountMethodsRepository;

@Service
@RequiredArgsConstructor
public class CountMethodsServiceImpl implements CountMethodsService {

    private final CountMethodsRepository countMethodsRepository;


    @Override
    public void addCallMethod(MethodDto methodDto) {
        if (!countMethodsRepository.findById(methodDto.getName()).isPresent()) {

            countMethodsRepository.save(Method.builder()
                    .name(methodDto.getName())
                    .countCalls(1L)
                    .build());
        } else {
            Method method1 = countMethodsRepository.getById(methodDto.getName());

            countMethodsRepository.save(Method.builder()
                    .name(methodDto.getName())
                    .countCalls(method1.getCountCalls() + 1L)
                    .build());
        }
    }
}
