package ru.pcs.web.controllers;


import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.pcs.web.dto.LessonDto;
import ru.pcs.web.dto.LessonsResponse;
import ru.pcs.web.services.LessonsService;

import java.time.LocalDate;

@RestController
@RequestMapping("/api/lessons")
@RequiredArgsConstructor
public class LessonController {

    private final LessonsService lessonsService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<LessonsResponse> getLessons(@RequestParam("page") int page, @RequestParam("size") int size) {
        return ResponseEntity.ok()
                .headers(httpHeaders -> httpHeaders.add("dateTime", LocalDate.now().toString()))
                .body(LessonsResponse.builder().data(lessonsService.getLessons(page, size)).build());
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<LessonDto> addLesson(@RequestBody LessonDto lessonDto) {
        return ResponseEntity.ok(lessonsService.addLesson(lessonDto));
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{lesson-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity<LessonDto> updateLesson(@PathVariable("lesson-id") Long lessonId, @RequestBody LessonDto lessonDto) {
        return ResponseEntity.ok(lessonsService.updateLesson(lessonId, lessonDto));
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{lesson-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteLesson(@PathVariable("lesson-id") Long lessonId) {
        lessonsService.deleteLesson(lessonId);
    }
}
