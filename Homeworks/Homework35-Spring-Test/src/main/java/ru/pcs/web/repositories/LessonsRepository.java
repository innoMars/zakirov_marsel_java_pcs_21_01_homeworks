package ru.pcs.web.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.models.Lesson;
import ru.pcs.web.models.Student;

public interface LessonsRepository extends JpaRepository<Lesson,Long> {
    Page<Lesson> findAllByIsDeletedIsNull(Pageable pageable);
}
