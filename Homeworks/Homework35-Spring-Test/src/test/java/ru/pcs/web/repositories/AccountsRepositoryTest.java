package ru.pcs.web.repositories;

import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import ru.pcs.web.models.Account;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureEmbeddedDatabase(type = AutoConfigureEmbeddedDatabase.DatabaseType.POSTGRES,
beanName = "dataSource",provider = AutoConfigureEmbeddedDatabase.DatabaseProvider.ZONKY,
refresh = AutoConfigureEmbeddedDatabase.RefreshMode.BEFORE_EACH_TEST_METHOD)
@Sql(scripts = {"classpath:schema.sql", "classpath:data.sql"})
class AccountsRepositoryTest {

    @Autowired
    private  AccountsRepository accountsRepository;


    @Test
    public void find_by_token(){
        assertTrue(accountsRepository.findByToken("1234567").isPresent());
    }

    @Test
    public void find_by_token_empty(){
        assertFalse(accountsRepository.findByToken("").isPresent());
    }

    @Test
    public void find_by_email(){
        assertTrue(accountsRepository.findByEmail("1@mail.com").isPresent());
    }

    @Test
    public void find_by_email_empty(){
        assertFalse(accountsRepository.findByEmail("").isPresent());
    }

}