package ru.pcs.web.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import ru.pcs.web.dto.CourseDto;
import ru.pcs.web.dto.StudentDto;
import ru.pcs.web.services.StudentsService;

import java.util.Collections;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.Matchers.is;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@DisplayName("StudentController in working when ")
class StudentsControllerTest {

    @Autowired
    private MockMvc mockMvc;

//    @Autowired
////    private WebApplicationContext webApplicationContext;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private StudentsService studentsService;

    @BeforeEach
    public void SetUp() {
//        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        when(studentsService.getStudents(anyInt(), anyInt())).thenReturn(Collections.singletonList(StudentDto
                .builder()
                .firstName("Robert")
                .lastName("Fet")
                .build()));

        when(studentsService.updateStudent(Mockito.any(), Mockito.any()))
                .thenReturn(StudentDto.builder()
                        .id(3L)
                        .firstName("Rain")
                        .lastName("Ra")
                        .build());

        when(studentsService.addStudent(Mockito.any()))
                .thenReturn(StudentDto.builder()
                        .id(2L)
                        .firstName("Roma")
                        .lastName("Olya")
                        .build());

        when(studentsService.addCourseToStudent(anyLong(), Mockito.any()))
                .thenReturn(Collections.singletonList(CourseDto.builder()
                        .id(1L)
                        .title("Boom")
                        .build()));
    }

    @Nested
    @DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
    @DisplayName("getStudents() is working")
    class GetStudentsTest {

        @Test
        public void return_students_list() throws Exception {
            mockMvc.perform(get("/api/students")
                    .header("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxIiwicm9sZSI6IkFETUlOIiwic3RhdGUiOiJDT05GSVJNRUQiLCJlbWFpbCI6IjFAbWFpbC5jb20ifQ.Fds4i6mydwttBmuqLKNZiPMcdeY5ImZgpm6QlOy2lg0")
                    .param("page", "0")
                    .param("size", "1"))
                    .andDo(print())
                    .andExpect(status().isOk());
        }

        @Test
        public void return_correct_students_list() throws Exception {

            mockMvc.perform(get("/api/students")
                    .header("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxIiwicm9sZSI6IkFETUlOIiwic3RhdGUiOiJDT05GSVJNRUQiLCJlbWFpbCI6IjFAbWFpbC5jb20ifQ.Fds4i6mydwttBmuqLKNZiPMcdeY5ImZgpm6QlOy2lg0")
                    .param("page", "0")
                    .param("size", "1"))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("response.body.data.[0].firstName", is("Robert")))
                    .andExpect(jsonPath("response.body.data.[0].lastName", is("Fet")));

        }
    }

    @Nested
    @DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
    @DisplayName("addStudents() is working")
    class AddStudentsTest {
        @Test
        public void add_student_test() throws Exception {
            StudentDto studentDto = StudentDto.builder()
                    .id(2L)
                    .firstName("Roma")
                    .lastName("Olya")
                    .build();

            mockMvc.perform(post("/api/students")
                    .header("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxIiwicm9sZSI6IkFETUlOIiwic3RhdGUiOiJDT05GSVJNRUQiLCJlbWFpbCI6IjFAbWFpbC5jb20ifQ.Fds4i6mydwttBmuqLKNZiPMcdeY5ImZgpm6QlOy2lg0")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(studentDto)
                    ))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("response.body.firstName", is("Roma")))
                    .andExpect(jsonPath("response.body.lastName", is("Olya")))
                    .andDo(print());
        }
    }

    @Nested
    @DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
    @DisplayName("putStudents() is working")
    class PutStudentsTest {
        @Test
        public void put_student_test() throws Exception {
            StudentDto studentDto = StudentDto.builder()
                    .firstName("Rain")
                    .lastName("Ra")
                    .build();

            mockMvc.perform(put("/api/students/3")
                    .header("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxIiwicm9sZSI6IkFETUlOIiwic3RhdGUiOiJDT05GSVJNRUQiLCJlbWFpbCI6IjFAbWFpbC5jb20ifQ.Fds4i6mydwttBmuqLKNZiPMcdeY5ImZgpm6QlOy2lg0")
                    .content(objectMapper.writeValueAsString(studentDto))
                    .contentType(MediaType.APPLICATION_JSON)

            )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("response.body.firstName", is("Rain")))
                    .andExpect(jsonPath("response.body.lastName", is("Ra")))
                    .andDo(print());
        }
    }

    @Nested
    @DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
    @DisplayName("deleteStudents() is working")
    class DeleteStudentsTest {
        @Test
        public void delete_student_test() throws Exception {
            mockMvc.perform(delete("/api/students/2")
                    .header("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxIiwicm9sZSI6IkFETUlOIiwic3RhdGUiOiJDT05GSVJNRUQiLCJlbWFpbCI6IjFAbWFpbC5jb20ifQ.Fds4i6mydwttBmuqLKNZiPMcdeY5ImZgpm6QlOy2lg0")
                    .contentType(MediaType.APPLICATION_JSON)
            )
                    .andExpect(status().isAccepted())
                    .andDo(print());
        }
    }

    @Nested
    @DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
    @DisplayName("addStudents() is working")
    class addCourseToStudent {
        @Test
        public void add_course_to_student() throws Exception {
            CourseDto courseDto = CourseDto.builder()
                    .id(1L)
                    .title("Boom")
                    .build();

            mockMvc.perform(post("/api/students/2/courses")
                    .header("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxIiwicm9sZSI6IkFETUlOIiwic3RhdGUiOiJDT05GSVJNRUQiLCJlbWFpbCI6IjFAbWFpbC5jb20ifQ.Fds4i6mydwttBmuqLKNZiPMcdeY5ImZgpm6QlOy2lg0")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(courseDto)
                    ))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("response.body.data.[0].id", is(1)))
                    .andExpect(jsonPath("response.body.data.[0].title", is("Boom")))
                    .andDo(print());
        }
    }
}