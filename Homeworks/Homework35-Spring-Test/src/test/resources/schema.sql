drop table account;
create table account (
    id serial primary key,
    first_name varchar(255),
    last_name varchar(255),
    email varchar(255),
    password varchar(255),
    role varchar(255),
    state varchar(255),
    token varchar(255)
);
