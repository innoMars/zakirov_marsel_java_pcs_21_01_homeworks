package entity;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class FileProcessor {
    private static int count = 0;
    private final String fileName;

    public FileProcessor(String fileName) {
        this.fileName = fileName;
    }

    public long downloadFilesAndSize(String path) {
        synchronized (fileName) {
            int countCheck = 10;

            try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
                String line;
                while (countCheck != 0) {
                    line = reader.readLine();
                    try (InputStream in = new URL(line).openStream()) {
                        count++;
                        Files.copy(in, Paths.get(
                                "Homeworks/Homework08-Threads" +
                                        "/src/downloadPackage/image"
                                        + count + ".png"));
                        countCheck--;
                    }
                }
            } catch (MalformedURLException e2) {
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
            return sizePath(path);
        }
    }

    public long sizePath(String path) {
        synchronized (fileName) {

            Path folder = Paths.get(path);
            try {
                return Files.walk(folder)
                        .map(Path::toFile)
                        .filter(File::isFile)
                        .mapToLong(File::length)
                        .sum();
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }
}


