import entity.FileProcessor;

import java.util.Scanner;
import java.util.concurrent.*;

public class ThreadsHome08 {
    public static void main(String[] args) {
        FileProcessor fileProcessor = new FileProcessor("Homeworks" +
                "/Homework08-Threads/src/downloadPackage/linkFiles.txt");
        ExecutorService executorService = Executors.newFixedThreadPool(11);
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();

        Callable<Long> downloadFilesAndSize1 = () -> fileProcessor.downloadFilesAndSize
                ("Homeworks/Homework08-Threads/src/downloadPackage");


        Future<Long> f1 = executorService.submit(downloadFilesAndSize1);
        Future<Long> f2 = executorService.submit(downloadFilesAndSize1);
        Future<Long> f3 = executorService.submit(downloadFilesAndSize1);
        Future<Long> f4 = executorService.submit(downloadFilesAndSize1);
        Future<Long> f5 = executorService.submit(downloadFilesAndSize1);
        Future<Long> f6 = executorService.submit(downloadFilesAndSize1);
        Future<Long> f7 = executorService.submit(downloadFilesAndSize1);
        Future<Long> f8 = executorService.submit(downloadFilesAndSize1);
        Future<Long> f9 = executorService.submit(downloadFilesAndSize1);
        Future<Long> f10 = executorService.submit(downloadFilesAndSize1);


        Future<?>[] tasks = {f1,f2,f3,f4,f5,f6,f7,f8,f9,f10};
        while (true) {
            int count = 0;
            for (Future<?> future : tasks) {
                if (future.isDone()) {
                    count++;
                }
            }
            if (count == tasks.length) {
                for (Future<?> task : tasks) {
                    try {
                        System.out.println(task.get());
                    } catch (InterruptedException | ExecutionException e) {
                        throw new IllegalArgumentException(e);
                    }
                }
                return;
            }
            executorService.shutdown();

        }
    }
}
