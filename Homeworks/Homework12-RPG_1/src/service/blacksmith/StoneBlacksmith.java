package service.blacksmith;

import entity.WeaponAb;

public interface StoneBlacksmith {
    void createStoneWeapon(WeaponAb weaponAb);
}
