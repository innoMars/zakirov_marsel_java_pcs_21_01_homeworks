package service.blacksmith;

import entity.WeaponAb;

public interface WoodBlacksmith {
    void createWoodWeapon(WeaponAb weaponAb);
}
