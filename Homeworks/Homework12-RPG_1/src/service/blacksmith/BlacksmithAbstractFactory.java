package service.blacksmith;

import service.weapon.Weapon;

public interface BlacksmithAbstractFactory {
    StoneBlacksmith createStoneBlacksmith();
    WoodBlacksmith createWoodBlacksmith();
    IronBlacksmith createIronBlacksmith();

}
