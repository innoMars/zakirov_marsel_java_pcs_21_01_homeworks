package service.blacksmith;

import entity.Sword;
import entity.WeaponAb;

import java.util.UUID;

public class BlacksmithAbstractFactoryImpl implements BlacksmithAbstractFactory {

    @Override
    public StoneBlacksmith createStoneBlacksmith() {
        return new StoneBlacksmithImpl("Stone blacksmith: " +
                UUID.randomUUID());
    }

    @Override
    public WoodBlacksmith createWoodBlacksmith() {
        return new WoodBlacksmithImpl("Wood blacksmith: " +
                UUID.randomUUID());
    }

    @Override
    public IronBlacksmith createIronBlacksmith() {
        return new IronBlacksmithImpl("Iron blacksmith: " +
                UUID.randomUUID());
    }

    private static class WoodBlacksmithImpl implements WoodBlacksmith {
        String nameBlacksmith;

        public WoodBlacksmithImpl(String nameBlacksmith) {
            this.nameBlacksmith = nameBlacksmith;
        }

        @Override
        public void createWoodWeapon(WeaponAb weaponAb) {
            if (!weaponAb.getMaterial().equals(WeaponAb.Material.WOOD)) {
                System.out.println("Вы пытаетесь создать заготовку не из того материала");
                return;
            }
            System.out.println("\nБыло создано оружие из дерева.");
            weaponAb.info();
        }


    }

    private static class StoneBlacksmithImpl implements StoneBlacksmith {
        String nameBlacksmith;

        public StoneBlacksmithImpl(String nameBlacksmith) {
            this.nameBlacksmith = nameBlacksmith;
        }

        @Override
        public void createStoneWeapon(WeaponAb weaponAb) {
            if (!weaponAb.getMaterial().equals(WeaponAb.Material.STONE)) {
                System.out.println("Вы пытаетесь создать заготовку не из того материала");
                return;
            }
            System.out.println("\nБыло создано оружие из камня.");
            weaponAb.info();
        }
    }

    private static class IronBlacksmithImpl implements IronBlacksmith {
        String nameBlacksmith;

        public IronBlacksmithImpl(String nameBlacksmith) {
            this.nameBlacksmith = nameBlacksmith;
        }

        @Override
        public void createIronWeapon() {
            //другой вариант создания
            WeaponAb ab = Sword.builder()
                    .name("Sword " + UUID.randomUUID())
                    .damage((int) (Math.random() * 900))
                    .build();
            System.out.println("\nБыло создано оружие из железа.");
            ab.info();
        }

        @Override
        public String toString() {
            return "IronBlacksmithImpl{" +
                    "nameBlacksmith='" + nameBlacksmith + '\'' +
                    '}';
        }
    }
}
