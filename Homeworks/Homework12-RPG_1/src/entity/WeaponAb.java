package entity;

public abstract class WeaponAb implements service.weapon.Weapon, Cloneable {
    public String name;
    public int damage;
    public Material material;

    public abstract void info();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public enum Material {
        WOOD, IRON, STONE
    }
}
