package entity;

public class Sword extends WeaponAb {
    private final Material material = Material.IRON;

    public Sword() {
    }

    private Sword(Builder builder) {
        this.name = builder.name;
        this.damage = builder.damage;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public void info() {
        System.out.println("Меч: " + name + "\nУрон: " + damage + "\nМатериал изготовления: " + material);
    }

    @Override
    public Sword clone() {
        Sword sword = new Sword();
        sword.name = this.name;
        sword.damage = this.damage;
        return sword;
    }

    public Material getMaterial() {
        return material;
    }

    public static class Builder {
        private String name;
        private int damage;

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder damage(int damage) {
            this.damage = damage;
            return this;
        }

        public Sword build() {
            return new Sword(this);
        }
    }
}
