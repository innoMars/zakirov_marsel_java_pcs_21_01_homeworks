package entity;

public class Bow extends WeaponAb {
    private final Material material = Material.WOOD;


    private Bow(Builder builder) {
        this.name = builder.name;
        this.damage = builder.damage;
    }

    public Bow() {
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public void info() {
        System.out.println("Лук: " + name + "\nУрон: " + damage + "\nМатериал изготовления: " + material);
    }

    @Override
    public Bow clone() {
        Bow bow = new Bow();
        bow.name = this.name;
        bow.damage = this.damage;
        return bow;
    }

    @Override
    public String toString() {
        return "Bow{" +
                "material=" + material +
                ", name='" + name + '\'' +
                ", damage=" + damage +
                '}';
    }

    public Material getMaterial() {
        return material;
    }

    public static class Builder {
        private String name;
        private int damage;

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder damage(int damage) {
            this.damage = damage;
            return this;
        }

        public Bow build() {
            return new Bow(this);
        }
    }
}
