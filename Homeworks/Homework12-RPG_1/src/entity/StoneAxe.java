package entity;

public class StoneAxe extends WeaponAb {
    private final Material material = Material.STONE;

    private StoneAxe(Builder builder) {
        this.name = builder.name;
        this.damage = builder.damage;
    }

    public StoneAxe() {
    }

    public static Builder builder() {
        return new Builder();
    }


    public StoneAxe clone() {
        StoneAxe stoneAxe = new StoneAxe();
        stoneAxe.name = this.name;
        stoneAxe.damage = this.damage;
        return stoneAxe;
    }

    @Override
    public void info() {
        System.out.println("Каменный топор: " + name + "\nУрон: " + damage + "\nМатериал изготовления: " + material);
    }

    public Material getMaterial() {
        return material;
    }

    public static class Builder {
        private String name;
        private int damage;

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder damage(int damage) {
            this.damage = damage;
            return this;
        }

        public StoneAxe build() {
            return new StoneAxe(this);
        }
    }
}
