import entity.Bow;
import entity.StoneAxe;
import entity.Sword;
import entity.WeaponAb;
import service.blacksmith.*;

public class RPG1Main {
    public static void main(String[] args) {
        WeaponAb bow = Bow.builder()
                .name("Лук Женьшень")
                .damage(456)
                .build();
        WeaponAb sword = Sword.builder()
                .name("Пересвет")
                .damage(677)
                .build();

        WeaponAb axe = StoneAxe.builder()
                .name("Каменное чудо")
                .damage(999)
                .build();

        BlacksmithAbstractFactory blacksmith = new BlacksmithAbstractFactoryImpl();
        IronBlacksmith ironBlacksmith = blacksmith.createIronBlacksmith();
        StoneBlacksmith stoneBlacksmith = blacksmith.createStoneBlacksmith();
        WoodBlacksmith woodBlacksmith = blacksmith.createWoodBlacksmith();
        sword.info();
        //тест создания не с тем материалом
        //woodBlacksmith.createWoodWeapon(((StoneAxe) axe).clone());
        stoneBlacksmith.createStoneWeapon(((StoneAxe) axe).clone());
        woodBlacksmith.createWoodWeapon(((Bow) bow).clone());
        //Другой вариант создания оружия. Без заготовки.
        ironBlacksmith.createIronWeapon();

    }
}
