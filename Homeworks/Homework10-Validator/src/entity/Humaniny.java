package entity;

import annotations.Max;
import annotations.Min;
import annotations.MinMaxLength;
import annotations.NotEmpty;

public class Humaniny {
    @NotEmpty(line = "")
    private String name;
    @Min(18)
    private int age;
    @MinMaxLength(minLen = 2,maxLen = 4)
    private String country;
    @Max(6666)
    private int salary;


    public Humaniny() {
    }

    public Humaniny(String name, int age, String country, int salary) {
        this.name = name;
        this.age = age;
        this.country = country;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Humaniny{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", country='" + country + '\'' +
                ", salary=" + salary +
                '}';
    }
}
