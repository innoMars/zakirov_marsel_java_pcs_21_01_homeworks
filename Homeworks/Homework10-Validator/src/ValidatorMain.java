import entity.Humaniny;
import service.Validator;

public class ValidatorMain {
    public static void main(String[] args) {
        Validator validator = new Validator();

        Humaniny humaninyPass = new Humaniny("Name", 18, "RU", 6666);

//        Humaniny humaninyTestMaxFail = new Humaniny("Name",19,"RU",66646);
//        Humaniny humaninyTestEmptyFail = new Humaniny("  ",19,"RU",666);
//        Humaniny humaninyTestMinFail = new Humaniny("Name",13,"RU",666);
//      Humaniny humaninyTestMaxMinLenFail = new Humaniny("Name",19,"RUSTEM",6666);
        validator.validate(humaninyPass);

    }
}
