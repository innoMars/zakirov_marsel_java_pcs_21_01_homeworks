package service;

import annotations.Max;
import annotations.Min;
import annotations.MinMaxLength;
import annotations.NotEmpty;

import java.lang.reflect.Field;


public class Validator {

    public void validate(Object form) {
        Field[] fields = form.getClass().getDeclaredFields();
        for (Field field : fields) {
            notEmptyAnnotationCheck(field, form);
            minAnnotationCheck(field, form);
            maxAnnotationCheck(field, form);
            minMaxLengthAnnotationCheck(field, form);
        }

    }

    private void minAnnotationCheck(Field field, Object form) {
        Min minValue = field.getAnnotation(Min.class);
        if (minValue != null) {
            int intValue = minValue.value();
            field.setAccessible(true);
            try {
                if (intValue > field.getInt(form)) {
                    throw new IllegalArgumentException("Минимальное значение 18");
                }
            } catch (IllegalAccessException e) {
                throw new IllegalArgumentException(e);
            }

        }
    }

    private void maxAnnotationCheck(Field field, Object form) {
        field.setAccessible(true);
        Max maxValue = field.getAnnotation(Max.class);
        if (maxValue != null) {
            int intValue = maxValue.value();
            try {
                if (intValue < field.getInt(form)) {
                    throw new IllegalArgumentException("Максимальное значение 6666");
                }
            } catch (IllegalAccessException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }

    private void minMaxLengthAnnotationCheck(Field field, Object form) {
        MinMaxLength minMaxLength = field.getAnnotation(MinMaxLength.class);
        if (minMaxLength != null) {
            int min = minMaxLength.minLen();
            int max = minMaxLength.maxLen();

            try {
                field.setAccessible(true);
                int fieldLen = field.get(form).toString().length();
                if (fieldLen < min || fieldLen > max) {
                    throw new IllegalArgumentException("Минимальная длина 2, Максимальная 4");
                }
            } catch (IllegalAccessException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }

    private void notEmptyAnnotationCheck(Field field, Object form) {
        NotEmpty notEmpty = field.getAnnotation(NotEmpty.class);
        if (notEmpty != null) {
            try {
                field.setAccessible(true);
                String stringValue = field.get(form).toString();
                if (stringValue.isBlank()) {
                    throw new IllegalArgumentException("Поле не должно быть пустым");
                }
            } catch (IllegalAccessException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }
}
