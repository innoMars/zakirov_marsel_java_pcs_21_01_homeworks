package ru.pcs.web.servlets;

import org.springframework.context.ApplicationContext;
import ru.pcs.web.dto.AccountDto;
import ru.pcs.web.filters.ColorFilter;
import ru.pcs.web.repositories.AccountsRepository;
import ru.pcs.web.repositories.AccountsRepositoryJdbcImpl;
import ru.pcs.web.services.ProfileService;
import ru.pcs.web.services.ProfileServiceImpl;


import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;


@WebServlet("/profile")
public class ProfileServlet extends HttpServlet {

    private static final int COLOR_COOKIE_MAX_AGE = 60 * 60 * 24 * 365;

    private ProfileService profileService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("context");
        this.profileService = springContext.getBean(ProfileService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pageColor = request.getParameter("color");

        if (pageColor != null) {
            request.setAttribute("color", pageColor);
            Cookie cookie = new Cookie(ColorFilter.COLOR_COOKIE_NAME,pageColor);
            cookie.setMaxAge(COLOR_COOKIE_MAX_AGE);
            response.addCookie(cookie);
        }
        String email = (String) request.getSession(true).getAttribute("email");
        AccountDto accountDto = profileService.getAccount(email);
        request.getSession(true).setAttribute("profile", accountDto);
        request.getRequestDispatcher("/jsp/profile.jsp").forward(request, response);
    }
}
