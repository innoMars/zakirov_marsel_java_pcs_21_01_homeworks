public class ArrayList<T> implements List<T> {
    private final int CAPACITY = 1;
    private Object[] array = new Object[CAPACITY];
    private int size = 0;

    @Override
    public void add(T element) {
        if (size == array.length) {
            resize();
            array[size++] = element;
        } else {
            array[size++] = element;
        }
    }


    @Override
    public T get(int index) {
        return (T) array[index];
    }

    @Override
    public Iterator<T> iterator() {
        return new ArrayListIterator();
    }

    public int size() {
        return size;
    }

    private void resize() {
        Object[] newArray = new Object[(size * 3 / 2) + 1];
        System.arraycopy(array, 0, newArray, 0, size);
        array = newArray;
    }

    private class ArrayListIterator implements Iterator<T> {
        private int current = 0;

        @Override
        public boolean hasNext() {
            return current < size;
        }

        @Override
        public T next() {
            return (T) array[current++];
        }
    }

}
