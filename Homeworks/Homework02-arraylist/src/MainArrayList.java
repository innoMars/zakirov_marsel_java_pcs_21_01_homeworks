

public class MainArrayList {
    public static void main(String[] args) {
        List<String> arrayList = new ArrayList<>();
        arrayList.add("Machine");
        arrayList.add("Doctor");
        arrayList.add("Dasha");
        arrayList.add("Melon");
        arrayList.add("Apple");
        arrayList.add("Car");
        arrayList.add("Card1");
        arrayList.add("Card2");
        arrayList.add("Card3");
        arrayList.add("Card4");

        Iterator<String> iterator = arrayList.iterator();

        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }

        System.out.println("Method get: "+ arrayList.get(3));


    }
}
