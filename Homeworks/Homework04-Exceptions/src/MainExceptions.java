import entity.UserDB;

public class MainExceptions {
    public static void main(String[] args) {

        UserDB userDB = new UserDB();
        userDB.signUp("dsadasd@", "fffffff3");
        userDB.signUp("homework04@email.ru", "exceptions");
        //Неверный емейл
//        userDB.signUp("dsadasd", "fffffff3");
        //Неверный пароль
//        userDB.signUp("dsadasd@", "f");


        userDB.signIn("dsadasd@", "fffffff3");
        //Неверная аутенфикация
//        userDB.signIn("dsadasd@", "ff3");


        System.out.println("\nТаблица Пользователей");
        for (int i = 0; i < userDB.getUserList().size(); i++) {
            System.out.println("Email: " + userDB.getUserList().get(i).getEmail() +
                    "   | Password: " + userDB.getUserList().get(i).getPassword());
        }

    }

}
