package entity;

import exceptions.BadEmailException;
import exceptions.BadPasswordException;
import exceptions.UserNotFoundException;
import service.UsersService;

import java.util.ArrayList;

public class UserDB implements UsersService {
    private final ArrayList<User> userList = new ArrayList<>();


    @Override
    public void signUp(String email, String password) {
        User user = new User();
        if (!email.contains("@")) {
            throw new BadEmailException("емейл пользователя должен содержать символ @");
        } else {
            user.setEmail(email);
        }
        if (password.length() < 7 && password.contains(" ")) {
            throw new BadPasswordException("пароль пользователя должен состоять из букв и цифр и спец. символов," +
                    " длина > 7");
        } else {
            user.setPassword(password);
        }
        userList.add(user);
    }

    @Override
    public void signIn(String email, String password) {
        boolean isContains = false;
        for (int i = 0; i < userList.size(); i++) {
            if (userList.get(i).getEmail().equals(email) &&
                    userList.get(i).getPassword().equals(password)) {
                isContains = true;
                System.out.println("Вы авторизованы под: " + userList.get(i).getEmail());
            }
        }
        if (!isContains) {
            throw new UserNotFoundException("Пользователь не найден или неверен пароль/емейл");
        }
    }

    @Override
    public String toString() {
        return "UserDB{" +
                "userList=" + userList +
                '}';
    }

    public ArrayList<User> getUserList() {
        return userList;
    }

}

