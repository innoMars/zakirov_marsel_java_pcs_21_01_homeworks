package com.pcs.config;


import java.util.Properties;

public class Config {
    private final int threadsCount;
    private final String dbUser;
    private final String dbPassword;
    private final String dbURL;
    private final String dbOrg;

    public Config() {
        try {
            Properties properties = new Properties();
            properties.load(getClass().getResourceAsStream("/propertiesFile.properties"));
            this.threadsCount = Integer.parseInt(properties.getProperty("executor.threads.count"));
            this.dbUser =properties.getProperty("db.user");
            this.dbPassword = properties.getProperty("db.password");
            this.dbURL = properties.getProperty("db.URL");
            this.dbOrg = properties.getProperty("db.org");
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    public int getThreadsCount() {
        return threadsCount;
    }

    public String getDbUser() {
        return dbUser;
    }

    public String getDbPassword() {
        return dbPassword;
    }

    public String getDbURL() {
        return dbURL;
    }

    public String getDbOrg() {
        return dbOrg;
    }

}
