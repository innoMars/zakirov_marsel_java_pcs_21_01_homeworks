package com.pcs.app.entity;

public class Product {
    private Long id;
    private String name;
    private Integer price;
    private Integer booking_id;

    public Product(String name, Integer price, Integer booking_id) {
        this.name = name;
        this.price = price;
        this.booking_id = booking_id;
    }

    public Product(Long id, String name, Integer price, Integer booking_id) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.booking_id = booking_id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(Integer booking_id) {
        this.booking_id = booking_id;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", booking_id=" + booking_id +
                '}';
    }
}
