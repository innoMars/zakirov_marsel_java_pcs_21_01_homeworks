package com.pcs.app;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.pcs.app.service.ProductRepositories;
import com.pcs.app.service.ProductRepositoriesImpl;
import com.pcs.config.Config;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Parameters(separators = "=")
public class MavenHM {
    @Parameter(names = {"--hikari-pool-size"})
    private int poolSize;

    public static void main(String[] args) {
        Config configFile = new Config();

        HikariConfig config = new HikariConfig();
        config.setUsername(configFile.getDbUser());
        config.setPassword(configFile.getDbPassword());
        config.setDriverClassName(configFile.getDbOrg());
        config.setJdbcUrl(configFile.getDbURL());
        config.setMaximumPoolSize(configFile.getThreadsCount());

        MavenHM mavenStart = new MavenHM();
        JCommander.newBuilder()
                .addObject(mavenStart)
                .build()
                .parse(args);
        if (mavenStart.poolSize != 0) {
            config.setMaximumPoolSize(mavenStart.poolSize);

        }

        HikariDataSource dataSource = new HikariDataSource(config);
        ProductRepositories productRepositories = new ProductRepositoriesImpl(dataSource);
        ExecutorService service = Executors.newFixedThreadPool(configFile.getThreadsCount());

        for (int i = 0; i < configFile.getThreadsCount(); i++) {
            service.submit(() -> {
                for (int j = 0; j < 10; j++) {
                    try {
                        System.out.println(productRepositories.findAll(j, 10));
                        System.out.println(mavenStart.poolSize);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        service.shutdown();


    }
}