import chain.*;
import strategy.FileReadClass;
import strategy.StreamRead;

public class FileAnalysisMain {
    public static void main(String[] args) {

        NumberDivisionListInFilesImpl numberDivisionListInFiles = new NumberDivisionListInFilesImpl();
        ModelBaseDivisionListInFilesImpl modelBaseDivisionListInFiles = new ModelBaseDivisionListInFilesImpl();
        ColorBaseDivisionListInFilesImpl colorBaseDivisionListInFiles = new ColorBaseDivisionListInFilesImpl();
        OdometerBaseDivisionListInFilesImpl odometerBaseDivisionListInFiles = new OdometerBaseDivisionListInFilesImpl();
        PriceBaseDivisionListInFilesImpl priceBaseDivisionListInFiles = new PriceBaseDivisionListInFilesImpl();
        FileReadClass fileReadClass = new FileReadClass
                ("Homeworks/Homework13-FileAnalysis/src/carListAll.txt", numberDivisionListInFiles);
        numberDivisionListInFiles.setNextDivision(modelBaseDivisionListInFiles);
        modelBaseDivisionListInFiles.setNextDivision(colorBaseDivisionListInFiles);
        colorBaseDivisionListInFiles.setNextDivision(odometerBaseDivisionListInFiles);
        odometerBaseDivisionListInFiles.setNextDivision(priceBaseDivisionListInFiles);
        fileReadClass.setReadFile(new StreamRead());
        fileReadClass.onStolen(fileReadClass::stolenCars);
        fileReadClass.onWrongFormat(fileReadClass::wrongFormatNumberCar);
        fileReadClass.readFile();
        fileReadClass.printFile();
    }
}
