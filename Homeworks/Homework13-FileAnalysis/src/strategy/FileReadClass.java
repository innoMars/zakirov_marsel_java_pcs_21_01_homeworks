package strategy;

import chain.DivisionListInFiles;
import entity.CarObject;
import observer.FormatNumberCar;
import observer.Stolen;

import java.io.*;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class FileReadClass {

    private String filename;
    private List<CarObject> carObjects;
    private ReadFile readFile;
    private FormatNumberCar formatNumberCar;
    private Stolen stolen;
    private final DivisionListInFiles firstChain;
    public FileReadClass(String filename, DivisionListInFiles firstChain) {
        this.filename = filename;
        this.firstChain = firstChain;
    }


    public void onStolen(Stolen stolen) {
        this.stolen = stolen;
    }

    public void onWrongFormat(FormatNumberCar formatNumberCar) {
        this.formatNumberCar = formatNumberCar;
    }

    public void wrongFormatNumberCar() {
        try (BufferedWriter writer = new BufferedWriter(
                new FileWriter("Homeworks/Homework13-FileAnalysis/src/blackListCar.txt"))) {
            for (int i = 0; i < carObjects.size(); i++) {
                if (!carObjects.get(i).getNumber().matches("[A-Z]{1}\\d{3}[A-Z]{2}")) {
                    writer.write(
                            carObjects.get(i).getNumber() + " " + carObjects.get(i).getModel() + "\n");
                }
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void stolenCars() {
        List<CarObject> stolenCars = null;
        try (BufferedReader reader = new BufferedReader(
                new FileReader("Homeworks/Homework13-FileAnalysis/src/stolenCars.txt"))) {
            stolenCars = reader.lines().map(carMapFunction).collect(Collectors.toList());
            for (int i = 0; i < carObjects.size(); i++) {
                for (int j = 0; j < stolenCars.size(); j++) {
                    if (carObjects.get(i).getNumber().equals(stolenCars.get(j).getNumber())) {
                        System.out.println("Найдена машина в списк уганных: " + carObjects.get(i).getModel()
                                + " " + carObjects.get(i).getNumber());
                    }
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }


    }

    public List<CarObject> readFile() {
        carObjects = readFile.readFile(filename);
        firstChain.divisionFile(carObjects);
        stolen.stolenCars();
        formatNumberCar.correctNumberCar();
        return carObjects;
    }

    public void setReadFile(ReadFile readFile) {
        this.readFile = readFile;
    }

    public void printFile() {
        System.out.println(carObjects);
    }

    private final static Function<String, CarObject> carMapFunction = line -> {
        String[] parts = line.substring(1, line.length() - 1).split("]\\[");
        String number = parts[0];
        String model = parts[1];
        String color = parts[2];
        int odometer = Integer.parseInt(parts[3]);
        int price = Integer.parseInt(parts[4]);
        return new CarObject(number, model, color, odometer, price);
    };
}
