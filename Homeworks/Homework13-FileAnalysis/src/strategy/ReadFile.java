package strategy;

import entity.CarObject;

import java.util.List;

public interface ReadFile {
    List<CarObject> readFile(String filename);
}
