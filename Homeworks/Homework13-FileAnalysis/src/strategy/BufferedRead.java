package strategy;

import entity.CarObject;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class BufferedRead implements ReadFile {

    private final static Function<String, CarObject> carMapFunction = line -> {
        String[] parts = line.substring(1, line.length() - 1).split("]\\[");
        String number = parts[0];
        String model = parts[1];
        String color = parts[2];
        int odometer = Integer.parseInt(parts[3]);
        int price = Integer.parseInt(parts[4]);
        return new CarObject(number, model, color, odometer, price);
    };

    @Override
    public List<CarObject> readFile(String filename) {
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
           return reader.lines()
                   .map(carMapFunction)
                   .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
