package observer;

import entity.CarObject;

import java.util.List;

public interface Stolen {
     void stolenCars();

}
