package observer;

import entity.CarObject;

import java.util.List;

public interface FormatNumberCar {
    void correctNumberCar();
}
