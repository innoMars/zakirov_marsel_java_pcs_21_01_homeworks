package chain;

import entity.CarObject;

import java.util.List;

public abstract class BaseDivisionListInFiles implements DivisionListInFiles {
    protected DivisionListInFiles next;


    @Override
    public void setNextDivision(DivisionListInFiles division) {
        this.next =division;
    }

    protected void nextChain(List<CarObject> carObjectList){
        if (next!=null){
            next.divisionFile(carObjectList);
        }
    }
}
