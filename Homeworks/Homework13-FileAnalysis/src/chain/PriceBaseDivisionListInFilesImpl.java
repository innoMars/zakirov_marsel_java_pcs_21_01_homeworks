package chain;

import entity.CarObject;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class PriceBaseDivisionListInFilesImpl extends BaseDivisionListInFiles {
    @Override
    public void divisionFile(List<CarObject> objectList) {
        try(BufferedWriter writer = new BufferedWriter(
                new FileWriter("Homeworks/Homework13-FileAnalysis/src/chain/priceCars.txt"))) {
            for (int i = 0;i< objectList.size(); i++){
                writer.write(objectList.get(i).getPrice()+"\n");
            }
            nextChain(objectList);
        }catch (IOException e){
            throw new IllegalArgumentException("Нет машин для добавления");
        }
    }
}
