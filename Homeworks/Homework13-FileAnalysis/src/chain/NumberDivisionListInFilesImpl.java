package chain;

import entity.CarObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class NumberDivisionListInFilesImpl extends BaseDivisionListInFiles {
    @Override
    public void divisionFile(List<CarObject> objectList) {

    try(BufferedWriter writer = new BufferedWriter(
            new FileWriter("Homeworks/Homework13-FileAnalysis/src/chain/numbersCars.txt"))) {
        for (int i = 0;i< objectList.size(); i++){
            writer.write(objectList.get(i).getNumber()+"\n");
        }
            nextChain(objectList);
    }catch (IOException e){
        throw new IllegalArgumentException("Нет машин для добавления");
    }
    }


}
