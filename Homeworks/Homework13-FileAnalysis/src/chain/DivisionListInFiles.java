package chain;

import entity.CarObject;

import java.util.List;

public interface DivisionListInFiles {
    void divisionFile(List<CarObject> objectList);
    void setNextDivision(DivisionListInFiles division);

}
