package ru.pcs.jpa.repositories;


import ru.pcs.jpa.models.FileDto;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import java.util.UUID;

public class FileDtoRepositoryImpl implements FileDtoRepository {
    private final EntityManager entityManager;

    public FileDtoRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void save(String filePath) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        Path path = Paths.get(filePath);
        String fileName = path.getFileName().toString();
        String extension = fileName.substring(fileName.lastIndexOf("."));


        FileDto fileDto = FileDto.builder()
                .fileName(String.valueOf(path.getFileName()))
                .storageName(UUID.randomUUID()+extension)
                .size(path.toFile().length())
                .build();
        entityManager.persist(fileDto);
        transaction.commit();
    }

    @Override
    public List<FileDto> findByOrigName(String origName) {
        TypedQuery<FileDto> query = entityManager.createQuery("select fileDto from FileDto fileDto" +
                " where file_name = :original_file_name", FileDto.class)
                .setParameter("original_file_name", origName);
        return query.getResultList();
    }
}
