package ru.pcs.jpa.repositories;

import ru.pcs.jpa.models.FileDto;

import java.util.List;


public interface FileDtoRepository {
    void save(String filePath);
    List<FileDto> findByOrigName(String origName);
}
