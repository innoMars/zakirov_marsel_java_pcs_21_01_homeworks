package ru.pcs.jpa.app;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.pcs.jpa.repositories.FileDtoRepository;
import ru.pcs.jpa.repositories.FileDtoRepositoryImpl;

import javax.persistence.EntityManager;

public class HomeworkJpa {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        EntityManager entityManager = sessionFactory.createEntityManager();
        FileDtoRepository fileDtoRepository = new FileDtoRepositoryImpl(entityManager);

//        String filePath = "C:\\Users\\gnil\\Desktop\\1.png";
//
//        fileDtoRepository.save(filePath);
        System.out.println(fileDtoRepository.findByOrigName("1.png"));
    }
}
