import entity.Car;
import service.CarRepository;

import java.util.Comparator;

public class UsersStream {
    public static void main(String[] args) {
        CarRepository carRepository = new CarRepository("Homeworks/Homework07-UsersStream/carList2.txt");
//        System.out.println(carRepository.findAll());

        System.out.println("1) Номера всех автомобилей, имеющих черный цвет или нулевой пробег.");
        numbersOfCarsHaveBlackColor(carRepository);

        System.out.println("2) Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс.");
        countDistinctCarForThePrice(carRepository);

        System.out.println("3) Вывести цвет автомобиля с минимальной стоимостью.");
        minPriceColorCar(carRepository);

        System.out.println("4) Средняя стоимость Camry");
        avgCamryInList(carRepository);
    }

    public static void minPriceColorCar(CarRepository carRepository) {
        String color = carRepository.findAll()
                .stream()
                .min(Comparator.comparing(Car::getPrice))
                .map(Car::getColor).get();
        System.out.println(color);
    }

    public static void numbersOfCarsHaveBlackColor(CarRepository carRepository) {
        carRepository.findAll()
                .stream()
                .filter((p) -> p.getColor().equals("Black") || p.getOdometer() == 0)
                .map(Car::getNumber)
                .forEach(System.out::println);


    }


    public static void avgCamryInList(CarRepository carRepository) {
        double avgCamry = carRepository.findAll()
                .stream()
                .filter((p) ->
                        p.getModel().equals("Camry"))
                .mapToInt(Car::getPrice).average().getAsDouble();
        System.out.println(avgCamry);
    }

    public static void countDistinctCarForThePrice(CarRepository carRepository) {
        long count = carRepository.findAll()
                .stream()
                .filter((p) -> p.getPrice() >= 700000 && p.getPrice() <= 800000)
                .distinct()
                .count();
        System.out.println(count);
    }
}
