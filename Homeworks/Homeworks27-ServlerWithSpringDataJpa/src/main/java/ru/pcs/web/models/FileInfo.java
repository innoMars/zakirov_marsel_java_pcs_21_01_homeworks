package ru.pcs.web.models;

import lombok.*;

import javax.persistence.*;


@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity(name = "file_info")
public class FileInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Column(name = "original_file_name")
    private String originalFileName;

    @Column(name = "storage_file_name")
    private String storageFileName;
    private Long size;
    @Column(name = "mime_type")
    private String mimeType;
    private String description;

    @Column(name = "account_id")
    private Long accountId;
}
