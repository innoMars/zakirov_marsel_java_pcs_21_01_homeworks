package ru.pcs.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.pcs.web.models.FileInfo;

import java.util.List;
import java.util.Optional;


public interface FileInfoRepository extends JpaRepository<FileInfo,Long> {

    @Query(value = "select fileInfo from file_info fileInfo where fileInfo.originalFileName like %:origName% and " +
            "fileInfo.accountId = :idL")
    List<FileInfo> searchByOriginal(@Param("origName") String orig,@Param("idL") Long accountId);

    @Query(value = "select fileInfo from file_info fileInfo where fileInfo.storageFileName = :strgName")
    Optional<FileInfo> findByStorageName(@Param("strgName") String storageFileName);


}
