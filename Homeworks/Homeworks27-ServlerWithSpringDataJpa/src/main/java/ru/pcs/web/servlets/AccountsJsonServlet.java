package ru.pcs.web.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationContext;
import ru.pcs.web.dto.AccountDto;
import ru.pcs.web.dto.SignUpForm;
import ru.pcs.web.services.AccountsService;
import ru.pcs.web.services.SignUpService;


import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet("/accounts/json")
public class AccountsJsonServlet extends HttpServlet {

    private AccountsService accountsService;
    private SignUpService signUpService;

    private ObjectMapper objectMapper;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("context");
        this.accountsService = springContext.getBean(AccountsService.class);
        this.signUpService = springContext.getBean(SignUpService.class);
        this.objectMapper = new ObjectMapper();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<AccountDto> accounts = accountsService.searchByEmail(request.getParameter("email"));
        response.setContentType("application/json");
        String json = objectMapper.writeValueAsString(accounts);
        response.getWriter().println(json);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String body = request.getReader().readLine();
        SignUpForm signUpForm = objectMapper.readValue(body, SignUpForm.class);
        signUpService.signUp(signUpForm);
        response.getWriter().println(objectMapper.writeValueAsString(accountsService.getAll()));
    }
}
