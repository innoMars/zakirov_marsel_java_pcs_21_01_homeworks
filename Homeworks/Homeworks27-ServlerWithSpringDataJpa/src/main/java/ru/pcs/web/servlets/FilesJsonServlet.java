package ru.pcs.web.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import ru.pcs.web.dto.AccountDto;
import ru.pcs.web.dto.FileDto;
import ru.pcs.web.services.FilesService;


import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/files/json")
public class FilesJsonServlet extends HttpServlet {

    private FilesService filesService;
    private ObjectMapper objectMapper;





    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("context");
        this.filesService = springContext.getBean(FilesService.class);
        this.objectMapper = new ObjectMapper();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        AccountDto accountDto = (AccountDto) request.getSession(true).getAttribute("profile");


        List<FileDto> namesFiles = filesService.searchByOrig( request.getParameter("fileName")
                , accountDto.getId());

        response.setContentType("application/json");
        String json = objectMapper.writeValueAsString(namesFiles);
        response.getWriter().println(json);
    }


}
