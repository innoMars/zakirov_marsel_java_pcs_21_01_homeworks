package ru.pcs.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.pcs.web.models.Account;

import java.util.List;
import java.util.Optional;

/**
 * 04.10.2021
 * 24. JDBC Repository
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface AccountsRepository extends JpaRepository<Account,Long> {


    @Query(value ="select account from Account account where account.email = :strEmail")
    Optional<Account> findByEmail(@Param("strEmail") String email);

    @Query(value ="select account from Account account where account.email like %:strEmail%")
    List<Account> searchByEmail(@Param("strEmail")String email);

}
