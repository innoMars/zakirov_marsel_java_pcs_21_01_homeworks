package ru.pcs.web.servlets;

import org.springframework.context.ApplicationContext;
import ru.pcs.web.dto.AccountDto;
import ru.pcs.web.services.AccountsService;


import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 18.10.2021
 * 30. Java Web Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@WebServlet("/accounts")
public class AccountsServlet extends HttpServlet {

    private AccountsService accountsService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("context");
        this.accountsService = springContext.getBean(AccountsService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<AccountDto> accounts = accountsService.getAll();
        request.setAttribute("accounts", accounts);
        request.getRequestDispatcher("jsp/accounts.jsp").forward(request, response);
    }
}
