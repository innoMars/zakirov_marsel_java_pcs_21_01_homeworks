package ru.pcs.web.servlets;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import ru.pcs.web.dto.AccountDto;
import ru.pcs.web.dto.FileDto;
import ru.pcs.web.services.FilesService;


import javax.persistence.EntityManager;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.LogManager;


@WebServlet("/filesUpload")
@MultipartConfig

public class FilesUploadServlet extends HttpServlet {

    private FilesService filesService;



    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("context");
        this.filesService = springContext.getBean(FilesService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/jsp/filesUpload.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BufferedReader descriptionReader = new BufferedReader(
                new InputStreamReader(request.getPart("description").getInputStream()));
        Part filePart = request.getPart("file");

        AccountDto accountDto = (AccountDto) request.getSession(true).getAttribute("profile");

        FileDto form = FileDto.builder()
                .description(descriptionReader.readLine())
                .fileName(filePart.getSubmittedFileName())
                .size(filePart.getSize())
                .mimeType(filePart.getContentType())
                .fileStream(filePart.getInputStream())
                .accountId(accountDto.getId())
                .build();
        filesService.upload(form);

    }
}
