package ru.pcs.web.servlets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import ru.pcs.web.dto.FileDto;
import ru.pcs.web.services.FilesService;


import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.logging.LogManager;


// GET /files?fileName=1ca51244-d764-463d-ade4-ee9aba3d0ab6.png
@WebServlet("/files")
public class FilesServlet extends HttpServlet {
    private FilesService filesService;

    @Autowired
    private Environment environment;


    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("context");
        this.filesService = springContext.getBean(FilesService.class);
        this.filesService.setStoragePath(environment.getProperty("storage.path"));
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String storageFileName = request.getParameter("fileName");
        FileDto file = filesService.getFile(storageFileName);
        response.setContentType(file.getMimeType());
        response.setContentLength(file.getSize().intValue());
        response.setHeader("Content-Disposition", "fileName=\"" + file.getOriginalFileName() + "\"");
        filesService.writeFile(file, response.getOutputStream());
        response.flushBuffer();
    }
}
