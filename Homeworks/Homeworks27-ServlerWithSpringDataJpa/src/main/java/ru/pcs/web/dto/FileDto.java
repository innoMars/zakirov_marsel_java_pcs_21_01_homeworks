package ru.pcs.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.pcs.web.models.Account;
import ru.pcs.web.models.FileInfo;

import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 20.10.2021
 * 30. Java Web Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FileDto {
    private Long size;
    private String fileName;
    private String mimeType;
    private InputStream fileStream;
    private String description;
    private String originalFileName;
    private Long accountId;

    public static FileDto from(FileInfo fileInfo) {
        return FileDto.builder()
                .size(fileInfo.getSize())
                .fileName(fileInfo.getStorageFileName())
                .mimeType(fileInfo.getMimeType())
                .originalFileName(fileInfo.getOriginalFileName())
                .description(fileInfo.getDescription())
                .accountId(fileInfo.getAccountId())
                .build();
    }

    public static List<FileDto> from(List<FileInfo> fileInfoList) {
        return fileInfoList.stream().map(FileDto::from).collect(Collectors.toList());
    }
}
