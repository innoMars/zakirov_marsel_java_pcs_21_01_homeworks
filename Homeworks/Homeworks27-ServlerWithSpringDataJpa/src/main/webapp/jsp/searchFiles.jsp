
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Files</title>
</head>
<script>
    function searchFile(originalFileName) {
        let request = new XMLHttpRequest();
        request.open('GET', '/files/json?fileName=' + originalFileName);
        request.send();

        request.onload = function() {

            if (request.status !== 200) {
                alert("Ошибка!")
            } else {
                let html =
                    '<tr>' +
                    '<th>Original File Name</th>' +
                    '<th>Link</th>' +
                    '</tr>';

                let json = JSON.parse(request.response);

                for (let i = 0; i < json.length; i++) {
                    html += '<tr>'
                    html +=     '<td>' + json[i]['originalFileName'] + '</td>'
                    const url = "/files?fileName=" + json[i]['fileName'];
                    html += '<td><a href="' + url +'">'+ json[i]['originalFileName']+ ' </a></td>'
                    html += '</tr>'
                }
                document.getElementById('file_info_table').innerHTML = html;
            }
        }
    }
</script>
<body>
<label for="originalFileName">Search by file:</label>
<input id="originalFileName" type="text" placeholder="Enter file name..."
       onkeyup="searchFile(document.getElementById('originalFileName').value)">
<table id="file_info_table">
</table>
</body>
</html>
