import java.sql.*;

public class StartDBMain {
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "123";
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/StartDB";

    public static void main(String[] args) {

        //объеденяет значения таблиц кастомер и ордерс
        String query1 = "select * from orders INNER JOIN customer c on orders.customer_id = c.id";
        //подсчитывает сумму айдишников заказов)
        String query2 = "select sum(order_id) from orders INNER JOIN cheque c on orders.id = c.order_id";
        try (Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
             PreparedStatement statement = connection.prepareStatement(query2)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    // вывод для первого запроса
//                    System.out.println(resultSet.getInt("id") + " " + resultSet.getInt("id"));
                    System.out.println(resultSet.getInt("sum"));
                }
            }


        } catch (SQLException throwables) {
            throw new IllegalArgumentException(throwables);
        }
    }
    }
