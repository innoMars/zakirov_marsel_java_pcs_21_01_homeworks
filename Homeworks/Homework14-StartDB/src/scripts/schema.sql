create table product
(
id      serial primary  key,
price integer
);

create  table customer
(
id serial primary key,
customer_Name varchar(30)
);

create table orders
(
id serial primary key,
customer_id integer,
product_id integer ,
foreign key (customer_id) references customer(id),
foreign key (product_id) references product(id)
);

create table booking_product
(
id serial primary key,
product_id integer,
customer_id integer,
foreign key (product_id) references  product(id),
foreign key (customer_id) references  customer(id)
);
create table booking_order
(
id serial primary key,
order_id integer,
foreign key (order_id) references  orders(id)
);

create table cheque
(
id serial primary key,
order_id integer,
foreign key (order_id) references orders(id)
);