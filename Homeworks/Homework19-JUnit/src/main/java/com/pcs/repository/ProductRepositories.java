package com.pcs.repository;

import com.pcs.entity.Product;

import java.util.List;

public interface ProductRepositories {
    List<Product> findAll(int page, int size);
}
