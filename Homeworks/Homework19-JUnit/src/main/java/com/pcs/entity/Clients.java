package com.pcs.entity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Clients {
    private final ExecutorService executorService;
    private static final Logger logger = LoggerFactory.getLogger(Clients.class);
    private List<Runnable> tasks;

    public Clients() {
        this.executorService = Executors.newCachedThreadPool();
        this.tasks = new ArrayList<>();
        logger.info("Initialized thread pool...");
    }



    public void newClient(Runnable task) {
        this.tasks.add(task);
        logger.debug("Added task");
    }

    public void run() {
        for (Runnable task : tasks) {
            executorService.submit(task);
        }
        logger.info("Run clients");
    }

    public void stop() {
        executorService.shutdown();
        logger.info("Shutdown thread pool");
    }

}
