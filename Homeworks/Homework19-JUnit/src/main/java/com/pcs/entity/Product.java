package com.pcs.entity;

import java.util.Objects;

public class Product {
    private Long id;
    private String name;
    private Integer price;
    private Integer booking_id;

    public Product(String name, Integer price, Integer booking_id) {
        this.name = name;
        this.price = price;
        this.booking_id = booking_id;
    }

    public Product(Long id, String name, Integer price, Integer booking_id) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.booking_id = booking_id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(Integer booking_id) {
        this.booking_id = booking_id;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", booking_id=" + booking_id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(id, product.id) &&
                Objects.equals(name, product.name) &&
                Objects.equals(price, product.price) &&
                Objects.equals(booking_id, product.booking_id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, price, booking_id);
    }
}
