insert into product(id, name, price, booking_id)
VALUES (1, 'Meat', 300, 1);
insert into product(id, name, price, booking_id)
VALUES (2, 'Fish', 100, 2);
insert into product(id, name, price, booking_id)
VALUES (3, 'Milk', 200, 3);
insert into product(id, name, price, booking_id)
VALUES (4, 'Shirt', 400, 5);
insert into product(id, name, price, booking_id)
VALUES (5, 'Shoes', 500, 3);
insert into product(id, name, price, booking_id)
VALUES (6, 'Parrot', 600, 6);
insert into product(id, name, price, booking_id)
VALUES (7, 'Mangal', 700, 7);
insert into product(id, name, price, booking_id)
VALUES (8, 'Chair', 800, 8);
insert into product(id, name, price, booking_id)
VALUES (9, 'Smartphone', 900, 8);
