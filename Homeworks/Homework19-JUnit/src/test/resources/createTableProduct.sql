create table product
(
    id         bigint auto_increment primary key,
    name       varchar(30),
    price      bigint,
    booking_id bigint
);

-- select setval('product_id_seq',20,true);
