package com.pcs.repository;


import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import static org.junit.jupiter.api.Assertions.*;
import javax.sql.DataSource;
import java.sql.SQLException;
import static org.mockito.Mockito.*;

class ProductRepositoriesImplTest {
    private ProductRepositoriesImpl productRepositories;
    @Spy
    private DataSource da;


    public void setUp() {
        DataSource dataSource = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript("createTableProduct.sql")
                .addScript("createProductScript.sql")
                .build();
        this.productRepositories = new ProductRepositoriesImpl(dataSource);
    }
    public void badSetUp() {
        da = Mockito.spy(new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript("createTableProduct2.sql")
                .addScript("createProductScript2.sql")
                .build());

        this.productRepositories = new ProductRepositoriesImpl(da);
    }

    @Test
    public void findAll() {
        setUp();
        assertNotNull(productRepositories.findAll(0, 10));
    }

    @Test
    public void throw_SQL_exception() throws SQLException {
        badSetUp();
        when(da.getConnection()).thenThrow(new SQLException());
        assertThrows(IllegalArgumentException.class,() ->productRepositories.findAll(0,1));
    }

}
