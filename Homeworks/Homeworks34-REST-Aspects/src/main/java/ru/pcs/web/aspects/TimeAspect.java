package ru.pcs.web.aspects;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import ru.pcs.web.dto.WrapperResponse;

@Component
@Aspect
public class TimeAspect {
    @Around(value = "execution(* ru.pcs.web.controllers.StudentsController.*(..))")
        public ResponseEntity<WrapperResponse> addTimeToResponse(ProceedingJoinPoint joinPoint) throws Throwable {

            long before = System.currentTimeMillis();
            Object object = joinPoint.proceed();
            long after = System.currentTimeMillis();
            return ResponseEntity.ok(WrapperResponse.builder()
                    .response(object)
                    .time(after - before)
                    .build());
        }
}
