package ru.pcs.web.controllers;


import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.pcs.web.dto.*;
import ru.pcs.web.models.Lesson;

import ru.pcs.web.services.CoursesService;

import java.time.LocalDate;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api/courses")
@RequiredArgsConstructor
public class CourseController {

    private final CoursesService courseService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<CoursesResponse> getCourses(@RequestParam("page") int page, @RequestParam("size") int size) {
        return ResponseEntity.ok()
                .headers(httpHeaders -> httpHeaders.add("dateTime", LocalDate.now().toString()))
                .body(CoursesResponse.builder().data(courseService.getCourses(page, size))
                        .build());
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<CourseDto> addCourse(@RequestBody CourseDto courseDto) {
        return ResponseEntity.ok(courseService.addCourse(courseDto));
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{course-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity<CourseDto> updateCourse(@PathVariable("course-id") Long courseId, @RequestBody CourseDto courseDto) {
        return ResponseEntity.ok(courseService.updateCourse(courseId, courseDto));
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{course-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteCourse(@PathVariable("course-id") Long courseId) {
        courseService.deleteCourse(courseId);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{course-id}/lessons")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<LessonsResponse> addLessonToCourse(@PathVariable("course-id") Long courseId,
                                             @RequestBody Lesson lesson) {
        return ResponseEntity.ok(LessonsResponse.builder()
                .data(courseService.addLessonToCourse(courseId, lesson))
                .build());
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{course-id}/lessons")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity<LessonsResponse> deleteLessonInCourse(@PathVariable("course-id") Long courseId,
                                                @RequestBody Lesson lesson) {
        return ResponseEntity.ok(LessonsResponse.builder()
                .data(courseService.deleteLessonInCourse(courseId, lesson))
                .build());
    }
}
