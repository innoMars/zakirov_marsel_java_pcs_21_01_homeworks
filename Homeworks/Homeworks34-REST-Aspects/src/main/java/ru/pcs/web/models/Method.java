package ru.pcs.web.models;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity(name = "METHOD")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Method {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
    @Id
    private String name;

    @Column(name = "count_calls")
    private Long countCalls;
}
